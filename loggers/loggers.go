/**
 * @Author: p_starxxliu
 * @Date: 2021/4/20 4:08 下午
 */

package loggers

import (
	"strings"
	"sync"

	"chainmaker.org/chainmaker/transfer-web-server/loggers/log"
	"chainmaker.org/chainmaker/transfer-web-server/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	MODULE_MANAGER = "[MANAGER]"
	MODULE_SDK     = "[SDK]"
	MODULE_USER    = "[USER]"
	MODULE_PROM    = "[PROM]"
	MODULE_WXW     = "[WXW]"
	MODULE_GIN     = "[GIN]"
)

var (
	loggers = make(map[string]*zap.SugaredLogger)
	// map[module-name]map[module-name+chainId]zap.AtomicLevel
	loggerLevels = make(map[string]map[string]zap.AtomicLevel)
	loggerMutex  sync.Mutex
	logConfig    *config.LogConfig
)

// SetLogConfig - 设置Log配置对象
func SetLogConfig(config *config.LogConfig) {
	logConfig = config
}

// GetLogger - 获取Logger对象
func GetLogger(name string) *zap.SugaredLogger {
	return GetLoggerByChain(name, "")
}

// GetLoggerByChain - 获取带链标识的Logger对象
func GetLoggerByChain(name, chainId string) *zap.SugaredLogger {
	loggerMutex.Lock()
	defer loggerMutex.Unlock()

	var config log.LogConfig
	var pureName string

	logHeader := name + chainId
	logger, ok := loggers[logHeader]
	if !ok {
		if logConfig == nil {
			logConfig = DefaultLogConfig()
		}

		if logConfig.LogLevelDefault == "" {
			defaultLogNode := GetDefaultLogNodeConfig()
			config = log.LogConfig{
				Module:       "[DEFAULT]",
				ChainId:      chainId,
				LogPath:      defaultLogNode.FilePath,
				LogLevel:     log.GetLogLevel(defaultLogNode.LogLevelDefault),
				MaxAge:       defaultLogNode.MaxAge,
				RotationTime: defaultLogNode.RotationTime,
				JsonFormat:   false,
				ShowLine:     true,
				LogInConsole: defaultLogNode.LogInConsole,
				ShowColor:    defaultLogNode.ShowColor,
			}
		} else {
			pureName = strings.ToLower(strings.Trim(name, "[]"))
			value, exists := logConfig.LogLevels[pureName]
			if !exists {
				value = logConfig.LogLevelDefault
			}
			config = log.LogConfig{
				Module:       name,
				ChainId:      chainId,
				LogPath:      logConfig.FilePath,
				LogLevel:     log.GetLogLevel(value),
				MaxAge:       logConfig.MaxAge,
				RotationTime: logConfig.RotationTime,
				JsonFormat:   false,
				ShowLine:     true,
				LogInConsole: logConfig.LogInConsole,
				ShowColor:    logConfig.ShowColor,
			}
		}

		var level zap.AtomicLevel
		logger, level = log.InitSugarLogger(&config)
		loggers[logHeader] = logger
		if pureName != "" {
			if _, exist := loggerLevels[pureName]; !exist {
				loggerLevels[pureName] = make(map[string]zap.AtomicLevel)
			}
			loggerLevels[pureName][logHeader] = level
		}
	}

	return logger
}

func RefreshLogConfig(config *config.LogConfig) {
	loggerMutex.Lock()
	defer loggerMutex.Unlock()

	// scan loggerLevels and find the level from config, if can't find level, set it to default
	for name, loggers := range loggerLevels {

		var (
			logLevevl zapcore.Level
			strlevel  string
			exist     bool
		)

		if strlevel, exist = config.LogLevels[name]; !exist {
			strlevel = config.LogLevelDefault
		}

		switch log.GetLogLevel(strlevel) {
		case log.LEVEL_DEBUG:
			logLevevl = zap.DebugLevel
		case log.LEVEL_INFO:
			logLevevl = zap.InfoLevel
		case log.LEVEL_WARN:
			logLevevl = zap.WarnLevel
		case log.LEVEL_ERROR:
			logLevevl = zap.ErrorLevel
		default:
			logLevevl = zap.InfoLevel
		}

		for _, aLevel := range loggers {
			aLevel.SetLevel(logLevevl)
		}
	}
}

// DefaultLogConfig - 获取默认Log配置
func DefaultLogConfig() *config.LogConfig {
	defaultLogNode := GetDefaultLogNodeConfig()
	config := &config.LogConfig{
		LogLevelDefault: defaultLogNode.LogLevelDefault,
		FilePath:        defaultLogNode.FilePath,
		MaxAge:          defaultLogNode.MaxAge,
		RotationTime:    defaultLogNode.RotationTime,
		LogInConsole:    defaultLogNode.LogInConsole,
	}
	return config
}

func GetDefaultLogNodeConfig() config.LogConfig {
	return config.LogConfig{
		LogLevelDefault: log.DEBUG,
		FilePath:        "./default.log",
		MaxAge:          log.DEFAULT_MAX_AGE,
		RotationTime:    log.DEFAULT_ROTATION_TIME,
		LogInConsole:    true,
		ShowColor:       true,
	}
}
