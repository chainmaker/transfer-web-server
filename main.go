package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"chainmaker.org/chainmaker/transfer-web-server/config"
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"chainmaker.org/chainmaker/transfer-web-server/handler"
	"chainmaker.org/chainmaker/transfer-web-server/loggers"
	"chainmaker.org/chainmaker/transfer-web-server/router"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var (
	logger   *zap.SugaredLogger
	blockNum int
)

func main() {
	var configPath, sqlitePath string

	flag.StringVar(&configPath, "configPath", "./config_file", "The path is config path (if set)")
	flag.StringVar(&sqlitePath, "sqlPath", "./sqlitedb/sqlite.db", "The path is sqlite db path (if set)")

	flag.Parse()

	//配置初始化
	config.InitConfig(configPath, config.GetConfigEnv())

	//mysql init
	//mysql.StartMysqlConnect()

	//sqlite init
	sqlite.StartSqliteConnect(sqlitePath)

	//redis init
	//redis.RedisInit()

	//日志库初始化
	loggers.SetLogConfig(&config.KaiwuConfig.LogConfig)
	logger = loggers.GetLogger(loggers.MODULE_GIN)

	//rate_limit 初始化
	handler.CreatRate()

	// 启动Web服务
	gin.SetMode(gin.ReleaseMode)
	ginRouter := gin.New()
	//pprof.Register(ginRouter)

	//authHandle := handler.GinToken(logger.Desugar())
	//ginRouter.Use(handler.GinRate(logger.Desugar()), handler.GinLogger(logger.Desugar()),handler.Cors(),
	//	handler.GinRecovery(logger.Desugar(), true))

	ginRouter.Use(handler.Cors(), handler.GinLogger(logger.Desugar()))
	//ginRouter.Use( handler.GinLogger(logger.Desugar()))

	router.InitRouter(ginRouter)

	// new an error channel to receive errors
	errorC := make(chan error, 1)

	// handle exit signal in separate go routines
	go handleExitSignal(errorC)

	_, cancel := context.WithCancel(context.Background())

	port := config.KaiwuConfig.CommonConfig.Port
	url := fmt.Sprintf(":%d", port)
	srv := &http.Server{
		Addr:    url,
		Handler: ginRouter,
	}

	go func() {
		logger.Infof("[TransferCenter] Listening and serving HTTP on port [%d]", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Fatalf("[MiddlewareCenter] startup failed, %s", err.Error())
		}
		logger.Infof("[KaiwuCenter] close listening on port [%d]", port)
	}()

	err := <-errorC
	if err != nil {
		logger.Error("transfer encounters error ", err)
	}

	if err := srv.Shutdown(context.Background()); err != nil {
		logger.Fatal("[TransferCenter] Server Shutdown:", err)
	}

	//mysql.Close()
	logger.Infof("close transfer sql ")
	//close child goroutine
	cancel()
	logger.Infof("close transfer service")
}

func handleExitSignal(exitC chan<- error) {

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, os.Interrupt, syscall.SIGINT)
	defer signal.Stop(signalChan)

	for sig := range signalChan {
		logger.Infof("received signal: %d (%s)", sig, sig)
		exitC <- nil
	}
}
