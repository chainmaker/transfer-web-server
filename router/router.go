/**
 * @Author: p_starxxliu
 * @Date: 2021/4/20 4:23 下午
 */

package router

import (
	"chainmaker.org/chainmaker/transfer-web-server/ctrl"
	_ "chainmaker.org/chainmaker/transfer-web-server/docs"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// 添加注释以描述 server 信息
// @title           Swagger TransferToolBridge API222
// @version         1.0
// @description     This is a transfer tool server .
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @host      localhost:12091
// @BasePath  /transfer

// @securityDefinitions.basic  BasicAuth

func InitRouter(router *gin.Engine) {
	v2Router := router.Group("/transfer")
	{
		v2Router.GET("/tasks/banner", ctrl.GetBannerTasks)                   //查询banner 首页信息
		v2Router.POST("/tasks/query", ctrl.GetTasks)                         //查询迁移全部信息，包括根据条件查询，支持分页
		v2Router.GET("/transfer_log/:task_id", ctrl.TransferLog)             //查询迁移日志
		v2Router.GET("/task_info/:task_id", ctrl.GetTaskInfo)                //查询迁移任务信息，注意对应的文件下载
		v2Router.GET("/download", ctrl.DownloadFile)                         //查询迁移任务信息，注意对应的文件下载
		v2Router.GET("/test_origin_chain/:task_id", ctrl.TestOriginChainSdk) //查询迁移任务信息，注意对应的文件下载
		v2Router.POST("/create_task", ctrl.CreateTask)                       //向用户返回唯一的任务Id
		v2Router.POST("/update_task", ctrl.UpdateTask)                       //向用户返回唯一的任务Id

		v2Router.POST("/create_task/add_origin_chain", ctrl.AddOriginChain)       //添加原链信息
		v2Router.POST("/create_task/update_origin_chain", ctrl.UpdateOriginChain) //修改原链信息
		v2Router.POST("/create_task/add_target_chain", ctrl.AddTargetChain)       //添加目标链信息
		v2Router.POST("/create_task/update_target_chain", ctrl.UpdateTargetChain) //修改目标链信息
		v2Router.GET("/start_task/:task_id", ctrl.StartTask)                      //启动任务
		v2Router.GET("/restart_task/:task_id", ctrl.ReStartTask)                  //重启任务
		v2Router.GET("/stop_task/:task_id", ctrl.StopTask)                        //停止任务
	}

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
