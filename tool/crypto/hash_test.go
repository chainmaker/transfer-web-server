/**
 * @Author: p_starxxliu
 * @Date: 2021/4/17 2:05 下午
 */
package crypto

import (
	"encoding/hex"
	"testing"
)

func TestGet(t *testing.T) {
	keyByte, _ := hex.DecodeString("this is key")

	keyHash, _ := HashCompile(HASH_TYPE_SM3, keyByte)

	keyHashBySha256, _ := HashCompile(HASH_TYPE_SHA256, keyByte)
	keyHashBySha3256, _ := HashCompile(HASH_TYPE_SHA3_256, keyByte)

	println(len(keyHash))          //32
	println(len(keyHashBySha256))  //32
	println(len(keyHashBySha3256)) //32
}

func TestHashCompile(t *testing.T) {
	by, _ := HashCompile(HASH_TYPE_SM3, []byte("pttnameptpassword"))
	println(hex.EncodeToString(by))
}
