/**
 * @Author: p_starxxliu
 * @Date: 2021/4/17 2:05 下午
 */

package crypto

import (
	"crypto"
	"crypto/sha256"
	"fmt"
	"hash"

	"github.com/tjfoc/gmsm/sm3"
	"golang.org/x/crypto/sha3"
)

type HashType uint

const (
	HASH_TYPE_SM3      HashType = 20
	HASH_TYPE_SHA256   HashType = HashType(crypto.SHA256)
	HASH_TYPE_SHA3_256 HashType = HashType(crypto.SHA3_256)
)

type Hash struct {
	hashType HashType
}

func (h *Hash) Get(data []byte) ([]byte, error) {
	var hf func() hash.Hash

	switch h.hashType {
	//case HASH_TYPE_MD5:
	//	hf = md5.New
	case HASH_TYPE_SM3:
		hf = sm3.New
	case HASH_TYPE_SHA256:
		hf = sha256.New
	case HASH_TYPE_SHA3_256:
		hf = sha3.New256
	default:
		return nil, fmt.Errorf("unknown hash algorithm")
	}

	f := hf()

	f.Write(data)
	return f.Sum(nil), nil
}

func HashCompile(hashType HashType, data []byte) ([]byte, error) {
	h := Hash{
		hashType: hashType,
	}
	return h.Get(data)
}
