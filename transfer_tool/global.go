package transfer_tool

import (
	"fmt"
	"sync"

	"chainmaker.org/chainmaker/transfer-tool/core"
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"go.uber.org/zap"
)

var (
	stopInfo        = "stop task"
	globalTransfers *GlobalTransfers
)

type GlobalTransfers struct {
	Globals   map[string]*core.TransferImpl
	GlobalErr map[string]chan error
	Lock      sync.Mutex
	log       *zap.SugaredLogger
}

func NewTransferTools(log *zap.SugaredLogger) *GlobalTransfers {
	if globalTransfers == nil {

		globalTransfers = &GlobalTransfers{
			Lock:      sync.Mutex{},
			Globals:   make(map[string]*core.TransferImpl, 0),
			GlobalErr: make(map[string]chan error, 0),
			log:       log,
		}
	}

	return globalTransfers
}

func (globals *GlobalTransfers) CloseAll() {
	globals.Lock.Lock()
	defer globals.Lock.Unlock()

	for _, errChan := range globals.GlobalErr {
		errChan <- nil
	}

	return
}

func (globals *GlobalTransfers) Close(taskId string) error {
	globals.Lock.Lock()
	defer globals.Lock.Unlock()
	errChan, ok := globals.GlobalErr[taskId]
	if !ok {
		return fmt.Errorf("task %s is not run", taskId)
	}

	errChan <- fmt.Errorf(stopInfo)

	delete(globals.Globals, taskId)
	delete(globals.GlobalErr, taskId)
	return nil
}

func (globals *GlobalTransfers) GetTransfer(taskId string) (*core.TransferImpl, error) {
	globals.Lock.Lock()
	defer globals.Lock.Unlock()
	transfer, ok := globals.Globals[taskId]
	if !ok {
		return nil, fmt.Errorf("task is not run")
	}

	return transfer, nil
}

func (globals *GlobalTransfers) IsExist() bool {
	globals.Lock.Lock()
	defer globals.Lock.Unlock()

	if len(globals.Globals) > 0 {
		return true
	}

	return false
}

func (globals *GlobalTransfers) Add(errChan chan error, taskId string, transfer *core.TransferImpl) {
	globals.Lock.Lock()
	defer globals.Lock.Unlock()

	globals.Globals[taskId] = transfer
	globals.GlobalErr[taskId] = errChan

	closeFunc := func(taskId string) error {
		globals.Lock.Lock()
		defer globals.Lock.Unlock()
		_, ok := globals.GlobalErr[taskId]
		if !ok {
			return fmt.Errorf("task %s is not run", taskId)
		}

		delete(globals.Globals, taskId)
		delete(globals.GlobalErr, taskId)
		return nil
	}

	go func() {
		select {
		case err := <-errChan:
			taskInfo, errS := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), taskId)
			if errS != nil {
				globals.log.Errorf("query task info by taskId[%s] fail,err[%s]", taskId, errS)
			}
			if err == nil { //表示完成全部迁移工作，修改迁移状态
				taskInfo.Status = 3
				globals.log.Infof("%s transfer task finish", taskId)

			} else if err.Error() == stopInfo {
				taskInfo.Status = 4
				globals.log.Infof("%s transfer task stop", taskId)

			} else {
				taskInfo.Status = 5
				taskInfo.LogInfo = err.Error()
				globals.log.Infof("%s transfer task fail,err[%s]", taskId, err)

			}
			transfer.Stop()

			taskInfo.LastHeight, taskInfo.AlreadyHeight = transfer.GetCurrentTransferHeight()
			err = taskInfo.UpdateChain(sqlite.GetDB())
			if err != nil {
				globals.log.Errorf("update task %s fail.err[%s]", taskId, err)
			}
			err = closeFunc(taskId)
			if err != nil {
				globals.log.Errorf("close task %s fail.err[%s]", taskId, err)
			}
		}

	}()

}
