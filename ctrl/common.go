/**
 * @Author: starxxliu
 * @Date: 2021/8/14 1:00 下午
 */

package ctrl

import (
	"github.com/gin-gonic/gin"
	jwt_token "chainmaker.org/chainmaker/transfer-web-server/tool/jwt-token"
)

type Response struct {
	Code   int         `json:"code"`
	ErrMsg string      `json:"msg"`
	Result interface{} `json:"result"`
}


func WriteJSON(ctx *gin.Context, code int, msg string, result interface{}) {
	res := &Response{
		Code:   code,
		ErrMsg: msg,
		Result: result,
	}
	//errCode := http.StatusOK
	//if res.Code != errCode {
	//	errCode = http.StatusBadRequest
	//}
	ctx.JSON(code, res)
}

func getClaims(c *gin.Context) (*jwt_token.JWTClaims, error) {
	claimsData := c.Request.Header.Get(jwt_token.ClaimsKey)

	return jwt_token.CreateClaims([]byte(claimsData))
}
