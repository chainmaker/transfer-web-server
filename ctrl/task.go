package ctrl

import (
	"fmt"
	"strings"
	"time"

	"chainmaker.org/chainmaker/transfer-tool/core"
	"chainmaker.org/chainmaker/transfer-web-server/config"
	"chainmaker.org/chainmaker/transfer-web-server/config/transfer_config"
	"chainmaker.org/chainmaker/transfer-web-server/db"
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"chainmaker.org/chainmaker/transfer-web-server/loggers"
	"chainmaker.org/chainmaker/transfer-web-server/transfer_tool"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"
)

// CreateTask godoc
// @Summary      Add a task info
// @Description  添加迁移任务基本信息
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        task  body      ctrl.TaskReq  true  "Add task"
// @Success      200   {object}  ctrl.Response{result=ctrl.CreateTaskRes}
// @Failure      400   {object}  ctrl.Response
// @Failure      404   {object}  ctrl.Response
// @Failure      500   {object}  ctrl.Response
// @Router       /create_task [post]
func CreateTask(c *gin.Context) {
	var (
		req    = &TaskReq{}
		logger = loggers.GetLogger(loggers.MODULE_GIN)
		err    error
	)

	err = c.BindJSON(req)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	taskId := GetUUID()
	task := &sqlite.TaskInfo{
		TaskId:             taskId,
		TaskName:           req.TaskName,
		OriginChainType:    req.OriginChainType,
		OriginChainVersion: req.OriginChainVersion,
		TargetChainType:    req.TargetChainType,
		TargetChainVersion: req.TargetChainVersion,
		TransferHeight:     req.TransferHeight,
	}

	task.CreateTime = time.Now().Format("2006-01-02 15:04:05")

	db := sqlite.GetDB()
	task, err = task.InsertTaskInfo(db)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	WriteJSON(c, 200, "", &CreateTaskRes{TaskId: taskId})
}

// UpdateTask godoc
// @Summary      Add a task info
// @Description  添加迁移任务的基本信息
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        task  body      ctrl.UpdateTaskReq  true  "Add task"
// @Success      200   {object}  ctrl.Response{result=ctrl.CreateTaskRes}
// @Failure      400   {object}  ctrl.Response
// @Failure      404   {object}  ctrl.Response
// @Failure      500   {object}  ctrl.Response
// @Router       /update_task [post]
func UpdateTask(c *gin.Context) {
	var (
		req    = &UpdateTaskReq{}
		logger = loggers.GetLogger(loggers.MODULE_GIN)
		err    error
	)

	err = c.BindJSON(req)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	transder, _ := transfer_tool.NewTransferTools(logger).GetTransfer(req.TaskId)
	if transder != nil {
		err = fmt.Errorf("can not update task[%s] config. it is runing ", req.TaskId)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	db := sqlite.GetDB()
	task, err := sqlite.GetTaskInfoByTaskId(db, req.TaskId)

	task.TaskName = req.TaskName
	task.OriginChainType = req.OriginChainType
	task.OriginChainVersion = req.OriginChainVersion
	task.TargetChainType = req.TargetChainType
	task.TargetChainVersion = req.TargetChainVersion
	task.TransferHeight = req.TransferHeight
	if task.TransferHeight > task.LastHeight-1 {
		WriteJSON(c, 500, ErrCodeSystemInner.String(fmt.Errorf("TransferHeight is over origin chain lastest height, Please modify your transfer height not over fabric latest BlockHeight: %d", task.LastHeight-1)), nil)
		return
	}

	err = task.UpdateChain(db)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	WriteJSON(c, 200, "", &CreateTaskRes{TaskId: req.TaskId})
}

// GetTaskInfo godoc
// @Summary      得到任务详情
// @Description  查询迁移任务通过任务id
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        task_id  path      string  true  "task ID"
// @Success      200      {object}  ctrl.Response{result=ctrl.TaskInfo}
// @Failure      400      {object}  ctrl.Response
// @Failure      404      {object}  ctrl.Response
// @Failure      500      {object}  ctrl.Response
// @Router       /task_info/{task_id} [get]
func GetTaskInfo(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_GIN)
		err    error
		res    = &TaskInfo{}
	)

	taskId := c.Param("task_id")
	if taskId == "" {
		err = fmt.Errorf("task_id can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	res, err = getTaskInfo(taskId, logger)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	transfer, err := transfer_tool.NewTransferTools(logger).GetTransfer(taskId)
	if err != nil && err.Error() != "task is not run" {
		err = fmt.Errorf("can not start new transfer service. already has runing transfer service ")
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if transfer != nil {
		res.Task.LastHeight, res.Task.AlreadyHeight = transfer.GetCurrentTransferHeight()
		if res.Task.TransferHeight == 0 {
			res.LastHeight = res.Task.LastHeight
			res.AlreadyHeight = res.Task.AlreadyHeight
		}
	} else {
		res.LastHeight = res.Task.LastHeight
		res.AlreadyHeight = res.Task.AlreadyHeight
	}

	WriteJSON(c, 200, "", res)
}

//拿什么构建config.yml 配置文件

// StartTask     godoc
// @Summary      启动任务
// @Description  查询迁移任务通过任务id
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        task_id  path      string  true  "task ID"
// @Success      200      {object}  ctrl.Response
// @Failure      400      {object}  ctrl.Response
// @Failure      404      {object}  ctrl.Response
// @Failure      500      {object}  ctrl.Response
// @Router       /start_task/{task_id} [get]
func StartTask(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_GIN)
		err    error
		path   string
	)

	taskId := c.Param("task_id")
	if taskId == "" {
		err = fmt.Errorf("task_id can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	if transfer_tool.NewTransferTools(logger).IsExist() {
		err = fmt.Errorf("can not start new transfer service. already has runing transfer service ")
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	taskInfo, err := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), taskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if taskInfo.Status == 0 {
		err = fmt.Errorf("config file not ready")
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
	}

	if taskInfo.TransferConfigPath == "" {
		path, _, err = transfer_config.CreateConfig(taskId)
		if err != nil {
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}

		taskInfo.TransferConfigPath = path
		err = taskInfo.UpdateChain(sqlite.GetDB())
		if err != nil {
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}

	} else {
		path = taskInfo.TransferConfigPath
	}

	transferConfig, err := config.GetTransferConfig(path)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), transferConfig)
		return
	}

	dbImpl := db.NewTransferDBImpl()

	errChan := make(chan error, 1)
	transfer, err := core.NewTransfer(transferConfig, dbImpl, nil, errChan, taskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	transfer_tool.NewTransferTools(logger).Add(errChan, taskId, transfer)

	err = transfer.Start()
	if err != nil {
		logger.Error(err)
		errChan <- err
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	//transfer.Stop()

	taskInfo.Status = 2
	err = taskInfo.UpdateChain(sqlite.GetDB())
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	WriteJSON(c, 200, "", fmt.Sprintf("start transfer task[%s] success ", taskId))
}

// ReStartTask     godoc
// @Summary      重启任务
// @Description  查询迁移任务通过任务id
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        task_id  path      string  true  "task ID"
// @Success      200      {object}  ctrl.Response
// @Failure      400      {object}  ctrl.Response
// @Failure      404      {object}  ctrl.Response
// @Failure      500      {object}  ctrl.Response
// @Router       /restart_task/{task_id} [get]
func ReStartTask(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_GIN)
		err    error
	)

	taskId := c.Param("task_id")
	if taskId == "" {
		err = fmt.Errorf("task_id can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	if transfer_tool.NewTransferTools(logger).IsExist() {
		err = fmt.Errorf("can not start new transfer service. already has runing transfer service ")
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	taskInfo, err := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), taskId)
	if taskInfo.TransferConfigPath == "" && taskInfo.Status < 4 {
		err = fmt.Errorf("task[%s] status is not stop or config not ready", taskId)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	transferConfig, err := config.GetTransferConfig(taskInfo.TransferConfigPath)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), transferConfig)
		return
	}

	dbImpl := db.NewTransferDBImpl()

	errChan := make(chan error, 1)
	transfer, err := core.NewTransfer(transferConfig, dbImpl, nil, errChan, taskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	err = transfer.Start()
	if err != nil {
		if err.Error() == "already is latest height" || err.Error() == "start transfer height more than transfer height" {
			taskInfo.Status = 3
			taskInfo.UpdateChain(sqlite.GetDB())
		}
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	transfer_tool.NewTransferTools(logger).Add(errChan, taskId, transfer)

	taskInfo.Status = 2
	taskInfo.UpdateChain(sqlite.GetDB())

	WriteJSON(c, 200, "", fmt.Sprintf("restart transfer task[%s] success ", taskId))

}

// StopTask      godoc
// @Summary      停止任务
// @Description  查询迁移任务通过任务id
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        task_id  path      string  true  "task ID"
// @Success      200      {object}  ctrl.Response
// @Failure      400      {object}  ctrl.Response
// @Failure      404      {object}  ctrl.Response
// @Failure      500      {object}  ctrl.Response
// @Router       /stop_task/{task_id} [get]
func StopTask(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_GIN)
		err    error
	)

	taskId := c.Param("task_id")
	if taskId == "" {
		err = fmt.Errorf("channel_name can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	taskInfo, err := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), taskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if taskInfo.Status == 0 {
		err = fmt.Errorf("config file not ready")
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
	}

	err = transfer_tool.NewTransferTools(logger).Close(taskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	WriteJSON(c, 200, "", fmt.Sprintf("stop transfer task[%s] success ", taskId))
}

// TransferLog      godoc
// @Summary      查询任务的迁移日志
// @Description  查询迁移任务日志通过任务id
// @Tags         task
// @Accept       json
// @Produce      json
// @Param        task_id  path      string  true  "task ID"
// @Success      200      {object}  ctrl.Response{result=ctrl.TaskLog}
// @Failure      400      {object}  ctrl.Response
// @Failure      404      {object}  ctrl.Response
// @Failure      500      {object}  ctrl.Response
// @Router       /transfer_log/{task_id} [get]
func TransferLog(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_GIN)
		res    = &TaskLog{}
		err    error
	)

	taskId := c.Param("task_id")
	if taskId == "" {
		err = fmt.Errorf("task_id can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	taskInfo, err := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), taskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	res.Task = taskInfo
	res.LastHeight = taskInfo.LastHeight

	if taskInfo.TransferHeight != 0 {
		res.LastHeight = taskInfo.TransferHeight
	}

	if taskInfo.TransferConfigPath == "" || taskInfo.Status == 1 {
		WriteJSON(c, 200, "", res)
		return
	}

	if taskInfo.Status == 2 {
		tran, err := transfer_tool.NewTransferTools(logger).GetTransfer(taskId)
		if err != nil {
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}
		res.LastHeight, res.AlreadyHeight = tran.GetCurrentTransferHeight()

	}

	WriteJSON(c, 200, "", res)
}

func getTaskInfo(taskId string, logger *zap.SugaredLogger) (*TaskInfo, error) {
	var res = &TaskInfo{}

	taskInfo, err := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), taskId)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	res.Task = taskInfo
	oChain, err := sqlite.GetOriginChainInfoByTaskId(sqlite.GetDB(), taskId)
	if err != nil && err.Error() != "record not found" {
		logger.Error(err)
		return nil, err
	}

	res.UserName = oChain.UserName
	res.ChannelName = oChain.ChannelName
	res.OriginSdkPath = oChain.SdkPath
	res.OriginCertPath = oChain.CertPath

	tChain, err := sqlite.GetTargetChainInfoByTaskId(sqlite.GetDB(), taskId)
	if err != nil && err.Error() != "record not found" {
		logger.Error(err)
		return nil, err
	}

	res.OrgId = tChain.OrgId
	res.AuthType = tChain.AuthType
	res.TargetConfigPath = tChain.ConfigPath
	return res, nil
}

func GetUUID() string {
	return strings.Replace(uuid.New().String(), "-", "", -1)
}
