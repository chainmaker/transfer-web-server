package ctrl

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestDeCompressByZip(t *testing.T) {
	err := DeCompressByZip("./wx-org1.chainmaker.org.zip","../config_file/task_id")
	require.Nil(t, err)
}
