/**
 * @Author: p_starxxliu
 * @Date: 2021/4/20 8:37 下午
 */

package ctrl

import "fmt"

type ErrCode int

const (
	ErrCodeOK                      ErrCode = iota
	ErrCodeSystemInner             ErrCode = 1000 + iota
	ErrCodeParamInvalid            ErrCode = 1000 + iota
	ErrCodeOptionInvalid           ErrCode = 1000 + iota
	ErrCodeUserNameOrPassword      ErrCode = 1000 + iota
	ErrCodeUserAuth                ErrCode = 1000 + iota
	ErrCodeDataBaseFail            ErrCode = 1000 + iota
	ErrCodeInvalidUserAlertLevel   ErrCode = 1000 + iota
	ErrCodeInvalidWebhookUrlFormat ErrCode = 1000 + iota
	ErrCodeSendEmailFailed         ErrCode = 1000 + iota
	ErrCodeSendUserAlertFailed     ErrCode = 1000 + iota
	ErrCodeSendPromAlertFailed     ErrCode = 1000 + iota
	ErrCodeCreateSDKFailed         ErrCode = 1000 + iota
	ErrCodeSDKInvokeFailed         ErrCode = 1000 + iota
	ErrCodeSDKInnerFailed          ErrCode = 1000 + iota
	ErrCodeAgentQueryFailed        ErrCode = 1000 + iota
)

var ErrCodeName = map[ErrCode][]string{
	ErrCodeOK:          {"OK", "成功"},
	ErrCodeSystemInner: {"inner system error", "系统内部错误"},

	// 参数错误
	ErrCodeOptionInvalid:           {"invalid option ", "无效的操作"},
	ErrCodeParamInvalid:            {"invalid request param", "无效的交易请求参数"},
	ErrCodeUserNameOrPassword:      {"invalid user name or password", "无效的用户名or密码"},
	ErrCodeUserAuth:                {"invalid user auth", "无效的用户权限"},
	ErrCodeDataBaseFail:            {"inner database errors", "数据库错误"},
	ErrCodeInvalidUserAlertLevel:   {"invalid user alert level", "错误的用户告警级别取值"},
	ErrCodeInvalidWebhookUrlFormat: {"invalid webhook url format", "非法webhook url格式"},

	ErrCodeSendEmailFailed:     {"send email failed", "邮件发送失败"},
	ErrCodeSendUserAlertFailed: {"send user alert failed", "发送用户自定义告警失败"},
	ErrCodeSendPromAlertFailed: {"send prom alert failed", "转送Prom告警失败"},
	ErrCodeCreateSDKFailed:     {"create Sdk instance failed", "创建chainmaker sdk 实例错误"},
	ErrCodeSDKInvokeFailed:     {"sdk execute invoke function failed", "sdk 执行invoke方法错误"},
	ErrCodeSDKInnerFailed:      {"sdk inner failed", "sdk内部错误"},
	ErrCodeAgentQueryFailed:    {"agent query failed", "代理查询错误"},
}

func (e ErrCode) String(err error) string {
	if s, ok := ErrCodeName[e]; ok {
		if err != nil {
			return s[0] + ": " + err.Error()
		}
		return s[0]
	}

	return fmt.Sprintf("unknown error code %d", uint32(e))
}
