package ctrl

import (
	"archive/zip"
	"io"
	"os"
)

func DeCompressByZip(zipFile, dest string) error {
	if _, err := os.Stat(dest); err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(dest, 0755)
			if err != nil {
				return err
			}
		}
	}

	reader, err := zip.OpenReader(zipFile)
	if err != nil {
		return err
	}
	defer reader.Close()

	for _, file := range reader.File {

		if file.FileInfo().IsDir() {
			err = os.MkdirAll(dest+"/"+file.Name, 0755)
			if err != nil {
				return err
			}
			continue
		}

		rc, err := file.Open()
		if err != nil {
			return err
		}
		defer rc.Close()
		filename := dest + "/" + file.Name

		w, err := os.Create(filename)
		if err != nil {
			return err
		}
		defer w.Close()

		_, err = io.Copy(w, rc)
		if err != nil {
			return err
		}
	}
	return nil
}
