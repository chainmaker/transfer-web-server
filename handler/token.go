/**
 * @Author: p_starxxliu
 * @Date: 2021/4/19 3:42 下午
 */

package handler

import (
	jwt_token "chainmaker.org/chainmaker/transfer-web-server/tool/jwt-token"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

const (
	LoginInterface = "login"
)

// GinToken 验证和更新token的方法
func GinToken(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer c.Next()

		//检查是否是登录接口
		path := c.Request.URL.Path
		if strings.HasSuffix(path, LoginInterface) {
			return
		}

		token := c.Request.Header.Get("token")

		claims, token, err := jwt_token.VerifyAction(token)
		if err != nil {
			logger.Error(err.Error())
			c.String(http.StatusUnauthorized, err.Error()) // nolint: err check
			c.Abort()
			return
		} else if token != "" {
			c.Writer.Header().Add("token", token)
		}

		c.Request.Header.Set(jwt_token.ClaimsKey, claims.String())
	}
}
