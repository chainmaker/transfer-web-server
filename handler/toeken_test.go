/**
 * @Author: p_starxxliu
 * @Date: 2021/4/19 5:18 下午
 */
package handler

import (
	"testing"
	"time"

	jwt_token "chainmaker.org/chainmaker/transfer-web-server/tool/jwt-token"
)

func BenchmarkGinToken(b *testing.B) {
	claim := &jwt_token.JWTClaims{
		UserID:   100,
		Password: "password",
		Username: "username",
		FullName: "full_name",
	}
	_, err := jwt_token.GetToken(claim)
	if err != nil {
		panic("-----")
	}
	//b.StartTimer()
	//for i := 0;i< b.N; i++ {
	//	_,err := jwt_token.GetToken(claim)
	//	if err != nil{
	//		panic("-----")
	//	}
	//}
	//b.StopTimer()
}

func BenchmarkGinToken2(b *testing.B) {
	claim := &jwt_token.JWTClaims{
		UserID:   100,
		Password: "password",
		Username: "username",
		FullName: "full_name",
	}
	token, err := jwt_token.GetToken(claim)
	if err != nil {
		panic("-----")
	}
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		_, _, err := jwt_token.VerifyAction(token)
		if err != nil {
			panic("-----")
		}
	}
	b.StopTimer()

}

func TestTps(t *testing.T) {
	println(time.Second / 6100)
}
