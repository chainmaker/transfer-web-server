/**
 * @Author: p_starxxliu
 * @Date: 2021/4/19 8:30 下午
 */

package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"chainmaker.org/chainmaker/transfer-web-server/config"
	"go.uber.org/zap"
)

var whiteIp map[string]bool
var AllTrue bool

// GinWhiteIp 检查是否是有效的remoteIp
func GinWhiteIp(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		clientIp := c.ClientIP()

		ok := verifyIp(clientIp)
		if !ok {
			err := fmt.Errorf("%s is invalid remote ip ", clientIp)
			logger.Error(err.Error())
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
		}
		c.Next()
	}
}

func verifyIp(clientIp string) bool {
	if AllTrue{
		return true
	}
	ok :=  whiteIp[clientIp]
	return ok
}

func InitWhiteIp() {
	whiteIp = make(map[string]bool, len(config.KaiwuConfig.AuthConfig.Ips))

	for _, ip := range config.KaiwuConfig.AuthConfig.Ips {
		if ip == "0.0.0.0" {
			AllTrue = true
		}
		whiteIp[ip] = true
	}
}
