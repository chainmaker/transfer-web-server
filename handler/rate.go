/**
 * @Author: p_starxxliu
 * @Date: 2021/4/20 2:59 下午
 */

package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"chainmaker.org/chainmaker/transfer-web-server/config"
	"go.uber.org/zap"
	"golang.org/x/time/rate"
)

const (

	// middleware ratelimit config
	middlewareRateLimitDefaultTokenPerSecond  = 5000
	middlewareRateLimitDefaultTokenBucketSize = 5000
)

var middlewareRateLimiter *rate.Limiter

func GinRate(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		if middlewareRateLimiter != nil && !middlewareRateLimiter.Allow() {
			errMsg := fmt.Errorf("rejected by ratelimit, try later please")
			logger.Warn(errMsg.Error())
			c.String(http.StatusNotImplemented, errMsg.Error())
			c.Abort()
		}
		c.Next()
	}
}

func CreatRate() {
	tokenBucketSize := config.KaiwuConfig.RateConfig.TokenBucketSize
	tokenPerSecond := config.KaiwuConfig.RateConfig.TokenPerSecond

	if tokenBucketSize >= 0 && tokenPerSecond >= 0 {
		if tokenBucketSize == 0 {
			tokenBucketSize = middlewareRateLimitDefaultTokenBucketSize
		}

		if tokenPerSecond == 0 {
			tokenPerSecond = middlewareRateLimitDefaultTokenPerSecond
		}

		middlewareRateLimiter = rate.NewLimiter(rate.Limit(tokenPerSecond), tokenBucketSize)
	}
}
