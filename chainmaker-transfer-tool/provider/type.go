/**
 * @Author: starxxliu
 * @Date: 2022/3/28 5:43 下午
 */

package provider

import (
	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
)

type SendInfo struct {
	Height uint64
	Block  interface{}
}

type TempBlock struct {
	Height uint64
	//FBlock    *BlockInfo
	OBlock    interface{} //原链Block
	TxBatch   []*commonpb.Transaction
	Proposal  *consensuspb.ProposalBlock
	Names     []string
	Done      bool
	Err       error
	ExtraDate string
}

type TransferRes struct {
	ChainId   string
	TaskId    string
	Done      bool
	Err       error
	ExtraDate string
}
