/**
 * @Author: starxxliu
 * @Date: 2022/3/17 4:47 下午
 */

package provider

import (
	"context"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

type Fetch interface {
	UpdateState()
	Close()
	CloseSdk()
	NextRound()
	GetThresholdRound() int
	GetRoundNum() int
	Start(ctx context.Context) error
	IsFinish(h uint64) bool
	QueryBlockByHeight(height uint64) (interface{}, error)
	GetLastBlockHeight() (uint64, error)
}

type Parse interface {
	ParseBlock(block interface{}) (interface{}, error)
}

type Fetcher interface {
	Fetch
	Parse
}

type ChainMakerTxBatch interface {
	CreateFirstTxBatch(bs []interface{}) ([]*common.Transaction, error)
	CreateTxBatch(block interface{}) ([]*common.Transaction, error)
	GetUserCrtByte() []byte
	GetChainMakerClientUserCrtHash() []byte
}

type SimContractExec interface {
	CreateWriteKeyMap(txWriteKeyMap map[string]*common.TxWrite, wSet map[string][]byte) error
	CreateTxWriteKeyMap(txWriteKeyMap map[string]*common.TxWrite, wSet map[string][]byte, txRwSet interface{}) error
	GetOriginChainTimeStamp(block interface{}) (int64, error)
}
