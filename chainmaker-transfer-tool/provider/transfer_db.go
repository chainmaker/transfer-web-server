package provider

type TransferDB interface {
	ContractDB
	StatusDB
}

type ContractDB interface {
	GetContracts(taskId string) (map[string]uint64, error)
	InsertContract(taskId, name string, height uint64) error
}

type StatusDB interface {
	GetSaveHeight(taskId string) (uint64, error)
	GetNewPreHash(taskId string) (string, error)
	GetCertHash(taskId string) (string, error)
	InsertStatus(height uint64, newPre, taskId string) error
	UpdateSaveHeightAndNewPre(height uint64, newPre, taskId string) error
	UpdateUserCrtHash(crtHash, taskId string) error
}
