package provider

import "go.uber.org/zap"

type FetcherConfig struct {
	Height   uint64
	Pre      []byte
	ChainId  string
	UserName string
	Path     string
	Send     chan *SendInfo
	Logger   *zap.SugaredLogger
}

type CreateFetcherConfig func(*FetcherConfig)

func WithFetcherConfigHeight(height uint64) CreateFetcherConfig {
	return func(config *FetcherConfig) {
		config.Height = height
	}
}

func WithFetcherConfigPre(pre []byte) CreateFetcherConfig {
	return func(config *FetcherConfig) {
		config.Pre = pre
	}
}

func WithFetcherConfigChainId(chainId string) CreateFetcherConfig {
	return func(config *FetcherConfig) {
		config.ChainId = chainId
	}
}

func WithFetcherConfigUserName(userName string) CreateFetcherConfig {
	return func(config *FetcherConfig) {
		config.UserName = userName
	}
}

func WithFetcherConfigPath(path string) CreateFetcherConfig {
	return func(config *FetcherConfig) {
		config.Path = path
	}
}

func WithFetcherConfigSendChan(send chan *SendInfo) CreateFetcherConfig {
	return func(config *FetcherConfig) {
		config.Send = send
	}
}

func WithFetcherConfigLogger(log *zap.SugaredLogger) CreateFetcherConfig {
	return func(config *FetcherConfig) {
		config.Logger = log
	}
}
