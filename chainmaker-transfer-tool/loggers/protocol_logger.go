/**
 * @Author: starxxliu
 * @Date: 2021/11/29 9:17 下午
 */

package loggers

import (
	"go.uber.org/zap"
)

type CMLogger struct {
	zlog *zap.SugaredLogger
}

func NewProtocolLog(log *zap.SugaredLogger) *CMLogger {
	return &CMLogger{
		zlog: log,
	}
}

func (l *CMLogger) Debug(args ...interface{}) {
	l.zlog.Debug(args...)
}
func (l *CMLogger) Debugf(format string, args ...interface{}) {
	l.zlog.Debugf(format, args...)
}
func (l *CMLogger) Debugw(msg string, keysAndValues ...interface{}) {
	l.zlog.Debugw(msg, keysAndValues...)
}
func (l *CMLogger) Error(args ...interface{}) {
	l.zlog.Error(args...)
}
func (l *CMLogger) Errorf(format string, args ...interface{}) {
	l.zlog.Errorf(format, args...)
}
func (l *CMLogger) Errorw(msg string, keysAndValues ...interface{}) {
	l.zlog.Errorw(msg, keysAndValues...)
}
func (l *CMLogger) Fatal(args ...interface{}) {
	l.zlog.Fatal(args...)
}
func (l *CMLogger) Fatalf(format string, args ...interface{}) {
	l.zlog.Fatalf(format, args...)
}
func (l *CMLogger) Fatalw(msg string, keysAndValues ...interface{}) {
	l.zlog.Fatalw(msg, keysAndValues...)
}
func (l *CMLogger) Info(args ...interface{}) {
	l.zlog.Info(args...)
}
func (l *CMLogger) Infof(format string, args ...interface{}) {
	l.zlog.Infof(format, args...)
}
func (l *CMLogger) Infow(msg string, keysAndValues ...interface{}) {
	l.zlog.Infow(msg, keysAndValues...)
}
func (l *CMLogger) Panic(args ...interface{}) {
	l.zlog.Panic(args...)
}
func (l *CMLogger) Panicf(format string, args ...interface{}) {
	l.zlog.Panicf(format, args...)
}
func (l *CMLogger) Panicw(msg string, keysAndValues ...interface{}) {
	l.zlog.Panicw(msg, keysAndValues...)
}
func (l *CMLogger) Warn(args ...interface{}) {
	l.zlog.Warn(args...)
}
func (l *CMLogger) Warnf(format string, args ...interface{}) {
	l.zlog.Warnf(format, args...)
}
func (l *CMLogger) Warnw(msg string, keysAndValues ...interface{}) {
	l.zlog.Warnw(msg, keysAndValues...)
}

func (l *CMLogger) DebugDynamic(getStr func() string) {
	//if l.logLevel == log.LEVEL_DEBUG {
	str := getStr()
	l.zlog.Debug(str)
	//}
}
func (l *CMLogger) InfoDynamic(getStr func() string) {
	//if l.logLevel == log.LEVEL_DEBUG || l.logLevel == log.LEVEL_INFO {
	l.zlog.Info(getStr())
	//}
}
