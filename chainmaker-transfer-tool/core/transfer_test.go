package core

import (
	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"testing"

	"chainmaker.org/chainmaker/transfer-tool/common/msgbus"
	"chainmaker.org/chainmaker/transfer-tool/core/creator"
	"chainmaker.org/chainmaker/transfer-tool/core/simulation"
	"chainmaker.org/chainmaker/transfer-tool/core/store"
	"chainmaker.org/chainmaker/transfer-tool/loggers"

	storev2 "chainmaker.org/chainmaker/store/v2"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/db"
	"chainmaker.org/chainmaker/transfer-tool/db/sqlite"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"github.com/golang/protobuf/proto"
	commonf "github.com/hyperledger/fabric-protos-go/common"
	"github.com/stretchr/testify/require"
)

func initConfig() {
	config.InitConfig("../config_path", config.GetConfigEnv())
}

func TestNewFbTransfer(t *testing.T) {
	initConfig()

	sqlite.StartSqliteConnect("")
	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	errC := make(chan error, 1)
	taskId := ""

	//New message bus
	msg := msgbus.NewMessageBus()
	msgC := msgbus.NewMessageBus()

	ctx, _ := context.WithCancel(context.Background())

	transferDB := db.NewTransferDBImpl()
	tc := config.TransferConfig

	transferImpl := &TransferImpl{
		chainId: tc.Chain.ChainId,
		taskId:  taskId,
	}

	//storeImpl init
	contractList, err := transferDB.GetContracts(taskId)
	require.Nil(t, err)

	checker, err := getChecker(tc.OriginChain.ChainType, tc.OriginChain.ChainVersion, contractList)
	require.Nil(t, err)

	plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_SAVE))
	store, err := store.NewStoreImpl2(msgC, plog, errC, ctx, checker, transferDB, taskId)
	require.Nil(t, err)
	transferImpl.store = store

	saveHeight := uint64(0)

	newPre := ""

	sim, err := getSimContract(tc.OriginChain.ChainType, tc.OriginChain.ChainVersion)
	require.Nil(t, err)

	core, err := simulation.NewCoreEngine(saveHeight, []byte(newPre), errC, msg, msgC, sim) //通过这里传递相应的createWriteKeyMap and CreateTxWriteKeyMap
	require.Nil(t, err)
	transferImpl.core = core

	tBatch, err := getTxBatch(tc.OriginChain.ChainType, tc.OriginChain.ChainVersion, "", tc, nil)
	require.Nil(t, err)

	send := make(chan *provider.SendInfo, defaultBlocks) //这里需要修改，可以从外部get到
	fetcher, err := getBlockFetcher(tc.OriginChain.ChainType, tc.OriginChain.ChainVersion, newPre, saveHeight, send, tc)
	require.Nil(t, err)

	txCreator, err := creator.NewParserImpl2(msg, transferDB, core.Proposer, tBatch, fetcher, send, taskId)
	require.Nil(t, err)
	transferImpl.txCreator = txCreator

	fabric_blocks := []uint64{0, 1, 2, 197672, 304079, 1157984, 1158003}
	//fabric_blocks := []uint64{3469525}

	fBlocks, err := readFabricBlock(fabric_blocks)
	require.Nil(t, err)

	println(hex.EncodeToString(fBlocks[0].Data.Data[0]))
	tblocks, err := parseFabricBlock(transferImpl, fBlocks)
	require.Nil(t, err)
	tblocks, err = createTxBatch(transferImpl, tblocks)
	require.Nil(t, err)

	tblocks, err = proposing(transferImpl, tblocks)
	require.Nil(t, err)
	fmt.Println(hex.EncodeToString(tblocks[0].Proposal.Block.Header.PreBlockHash))
	//tblocks[0].Proposal.TxsRwSet["28d1632d683c91e721575d2bfb590f61f0cd18e99549aab56babbc43e41b8fcc"].TxWrites[1].Key = []byte("nft_user_key")
	//rwSetJ, _ := json.Marshal(tblocks[0].Proposal.TxsRwSet["9c6dbcbe72ec9f8eadff10c596a56a263ebb13b00028074a80431c13b4daeb1c"])
	//println(string(rwSetJ))
	//err = checkBlock(tblocks)
	require.Nil(t, err)

}

func TestCmTransfer(t *testing.T) {
	initConfig()

	sqlite.StartSqliteConnect("")
	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	errC := make(chan error, 1)
	taskId := ""

	transferDB := db.NewTransferDBImpl()
	tc := config.TransferConfig

	transfer, err := NewTransfer(tc, transferDB, nil, errC, taskId)
	require.Nil(t, err)

	txCreator := transfer.GetTxCreator()

	height := uint64(20)
	block, err := txCreator.GetFetcher().QueryBlockByHeight(height)
	require.Nil(t, err)

	pBlock, err := txCreator.GetFetcher().ParseBlock(block)
	require.Nil(t, err)

	txs, err := txCreator.GetTxBatch().CreateTxBatch(pBlock)
	require.Nil(t, err)

	tb := &provider.TempBlock{
		Height:  height,
		OBlock:  block,
		TxBatch: txs,
	}

	_, tb.Proposal, err = txCreator.GetProposer().Proposing(height, nil, tb)
	require.Nil(t, err)

	tb.Proposal.Block, err = txCreator.GetProposer().HandleProposedBlock(tb.Proposal)
	require.Nil(t, err)

}

func readFabricBlock(heights []uint64) ([]*commonf.Block, error) {
	blocks := make([]*commonf.Block, 0, len(heights))
	for _, height := range heights {
		fileName := "../config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
		blockByte, err := ioutil.ReadFile(fileName)
		if err != nil {
			return nil, err
		}

		block := &commonf.Block{}
		err = proto.Unmarshal(blockByte, block)
		if err != nil {
			return nil, err
		}

		blocks = append(blocks, block)
	}

	return blocks, nil
}

func parseFabricBlock(tf *TransferImpl, blocks []*commonf.Block) ([]*provider.TempBlock, error) {
	tempBlocks := make([]*provider.TempBlock, 0, len(blocks))
	for _, block := range blocks {
		binfo, err := tf.txCreator.GetFetcher().ParseBlock(block)
		if err != nil {
			return nil, err
		}
		tb := &provider.TempBlock{
			OBlock: binfo,
		}
		tempBlocks = append(tempBlocks, tb)
	}
	return tempBlocks, nil
}

func createTxBatch(tf *TransferImpl, blocks []*provider.TempBlock) ([]*provider.TempBlock, error) {
	for _, block := range blocks {
		txs, err := tf.txCreator.GetTxBatch().CreateTxBatch(block.OBlock)
		if err != nil {
			return nil, err
		}
		block.TxBatch = txs
	}
	return blocks, nil
}

func proposing(tf *TransferImpl, blocks []*provider.TempBlock) ([]*provider.TempBlock, error) {

	for _, block := range blocks {
		height := block.Height
		_, proposalBlock, err := tf.txCreator.GetProposer().Proposing(height, nil, block)
		if err != nil {
			return nil, err
		}

		pblock, err := tf.txCreator.GetProposer().HandleProposedBlock(proposalBlock)
		if err != nil {
			return nil, err
		}
		proposalBlock.Block = pblock
		block.Proposal = proposalBlock

	}
	return blocks, nil
}

func TestTargetChainBlock(t *testing.T) {
	initConfig()

	sqlite.StartSqliteConnect("")
	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	errC := make(chan error, 1)
	taskId := ""

	//New message bus
	msgC := msgbus.NewMessageBus()

	ctx, _ := context.WithCancel(context.Background())

	transferDB := db.NewTransferDBImpl()
	tc := config.TransferConfig

	//storeImpl init
	contractList, err := transferDB.GetContracts(taskId)
	require.Nil(t, err)

	checker, err := getChecker(tc.OriginChain.ChainType, tc.OriginChain.ChainVersion, contractList)
	require.Nil(t, err)

	plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_SAVE))
	store, err := store.NewStoreImpl2(msgC, plog, errC, ctx, checker, transferDB, taskId)
	require.Nil(t, err)

	height := 1
	block := store.GetBlockWithRWSetsByHeight(uint64(height))

	blockByte, err := proto.Marshal(block)
	require.Nil(t, err)

	fileName := "../config_path/testdata/chainMaker_" + strconv.Itoa(int(height)) + ".proto"
	err = ioutil.WriteFile(fileName, blockByte, 777)
	require.Nil(t, err)
}

func TestReadChainMakerBlock(t *testing.T) { //zxl_contract zxl_contract
	initConfig()
	plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_SAVE))
	bstore, err := storev2.NewFactory().NewStore(config.TransferConfig.Chain.ChainId,
		config.TransferConfig.Storage, plog, nil)
	require.Nil(t, err)

	b, err := bstore.GetBlockWithRWSets(4)
	require.Nil(t, err)

	println("height === ", b)
}

func TestTxDeail(t *testing.T) {
	txDeail := `{"tx_id":"9ae0fde1a1454e66997938e9d3aa8322841ee3ea0cec451db5537300f7b3b08d","tx_reads":[{"key":"Q29udHJhY3Q6YXNzZXQ=","contract_name":"CONTRACT_MANAGE"}],"tx_writes":[{"key":"Q29udHJhY3Q6YXNzZXQ=","value":"CgVhc3NldBIGdjEuMC4wGAYqqwEKFnd4LW9yZzEuY2hhaW5tYWtlci5vcmcQARogcFOp/gLroH9nbXOAq/NvItibqk5dU0wKcKYQXCxnp6YiI2NsaWVudDEuc2lnbi53eC1vcmcxLmNoYWlubWFrZXIub3JnKgZDTElFTlQyQGM2M2M2ZjRlNmM4NDVkMzJhYTY3NDA1MTA2Yzk1ODdmMDkwNjY1NzQ3ZTZkNWRiMGIwMmVlMjE3Yjk3ODhlYjY=","contract_name":"CONTRACT_MANAGE"},{"key":"Q29udHJhY3RCeXRlQ29kZTphc3NldA==","value":"N3q8ryccAAQierLI/0hfAAAAAACCAAAAAAAAAPqciwXiDBAASDJZAA==","contract_name":"CONTRACT_MANAGE"}]}`
	txRWSet := &commonpb.TxRWSet{}

	err := json.Unmarshal([]byte(txDeail), txRWSet)
	require.Nil(t, err)
}

func TestTransferImpl_GetOriginChainFetch(t *testing.T) {
	initConfig()

	fetcher, err := GetOriginChainFetch("fabric", "1.4", "ltzxinchain", "../config_path/dev/zxl_sdk_config.yml", "admin")
	require.Nil(t, err)

	block, err := fetcher.QueryBlockByHeight(0)
	require.Nil(t, err)

	b, ok := block.(*commonf.Block)
	require.Equal(t, true, ok)

	println(hex.EncodeToString(b.Header.DataHash))
}

func TestSDK(t *testing.T) {
	initConfig()

	fetcher, err := GetOriginChainFetch("fabric", "1.4", "mychannel", "../../originchainfile/fabric_sdk_config.yml", "admin")
	require.Nil(t, err)

	block, err := fetcher.QueryBlockByHeight(80)
	require.Nil(t, err)

	b, ok := block.(*commonf.Block)
	require.Equal(t, true, ok)

	println(hex.EncodeToString(b.Header.DataHash))
}
