/**
 * @Author: starxxliu
 * @Date: 2021/11/10 8:28 下午
 */

package common

import (
	"fmt"
	"time"

	"chainmaker.org/chainmaker/transfer-tool/provider"

	"chainmaker.org/chainmaker/common/v2/crypto/hash"
	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/transfer-tool/core/simulation/core/common/scheduler"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	"go.uber.org/zap"
)

type BlockBuilder struct {
	txScheduler *scheduler.TxScheduler
	chainId     string                 // chain id, to identity this chain
	identity    protocol.SigningMember // identity manager
	//chainConf   protocol.ChainConf     // chain config
	hashType string
	log      *zap.SugaredLogger
}

func NewBlockBuilder(chainId, hashType string, identity protocol.SigningMember, sim provider.SimContractExec,
	log *zap.SugaredLogger) *BlockBuilder {
	creatorBlock := &BlockBuilder{
		txScheduler: scheduler.NewTxScheduler(log, sim),
		chainId:     chainId,
		identity:    identity,
		hashType:    hashType,
		log:         log,
	}

	return creatorBlock
}

func (bb *BlockBuilder) GenerateNewBlock(proposingHeight uint64, preHash []byte, txBatch []*commonpb.Transaction, tblock *provider.TempBlock) (
	*commonpb.Block, map[string]*commonpb.TxRWSet, map[string][]*commonpb.ContractEvent, []int64, error) {

	timeLasts := make([]int64, 0)

	startT := time.Now()
	block, err := initNewBlock(proposingHeight, preHash, bb.identity, bb.chainId, nil, false)
	if err != nil {
		return block, nil, nil, timeLasts, err
	}

	if block == nil {
		bb.log.Errorf("generate new block failed, block is nil")
		return nil, nil, nil, timeLasts, fmt.Errorf("generate new block failed, block is nil")
	}

	txRWSetMap, _, err := bb.txScheduler.Schedule(block, txBatch, bb.hashType, tblock)
	if err != nil {
		return block, nil, nil, timeLasts, err
	}
	sendT := time.Now()
	timestamp, err := bb.txScheduler.Sim.GetOriginChainTimeStamp(tblock.OBlock)
	if err != nil {
		return block, nil, nil, timeLasts, err
	} else if timestamp != 0 {
		block.Header.BlockTimestamp = timestamp
	}

	err = FinalizeBlock(block, txRWSetMap, nil, bb.hashType, bb.log)
	if err != nil {
		return block, nil, nil, timeLasts, err
	}
	bb.log.Debugf("block[%d] txBatch[%d] execute Schedule spend time[%d]ms Finalize Block "+
		"spend time[%+v]", proposingHeight, len(txBatch), sendT.Sub(startT).Milliseconds(), time.Since(sendT))

	return block, txRWSetMap, nil, timeLasts, nil
}

func initNewBlock(
	height uint64,
	preHash []byte,
	identity protocol.SigningMember,
	chainId string,
	chainConf protocol.ChainConf, isConfigBlock bool) (*commonpb.Block, error) {
	// get node pk from identity
	proposer, err := identity.GetMember()
	if err != nil {
		return nil, fmt.Errorf("identity serialize failed, %s", err)
	}
	preConfHeight := uint64(0) //迁移过程中preConfigHeight是固定的
	// if last block is config block, then this block.preConfHeight is last block height
	//if utils.IsConfBlock(lastBlock) { //这里是固定的参数
	//	preConfHeight = lastBlock.Header.BlockHeight
	//}

	block := &commonpb.Block{
		Header: &commonpb.BlockHeader{
			ChainId:        chainId,
			BlockHeight:    height,
			PreBlockHash:   preHash,
			BlockHash:      nil,
			PreConfHeight:  preConfHeight,
			BlockVersion:   protocol.DefaultBlockVersion,
			DagHash:        nil,
			RwSetRoot:      nil,
			TxRoot:         nil,
			BlockTimestamp: utils.CurrentTimeSeconds(),
			Proposer:       proposer,
			ConsensusArgs:  nil,
			TxCount:        0,
			Signature:      nil,
		},
		Dag:            &commonpb.DAG{},
		Txs:            nil,
		AdditionalData: nil,
	}
	if isConfigBlock {
		block.Header.BlockType = commonpb.BlockType_CONFIG_BLOCK
	}
	return block, nil
}

func FinalizeBlock(
	block *commonpb.Block,
	txRWSetMap map[string]*commonpb.TxRWSet,
	aclFailTxs []*commonpb.Transaction,
	hashType string,
	logger *zap.SugaredLogger) error {

	if aclFailTxs != nil && len(aclFailTxs) > 0 {
		// append acl check failed txs to the end of block.Txs
		block.Txs = append(block.Txs, aclFailTxs...)
	}

	// TxCount contains acl verify failed txs and invoked contract txs
	txCount := len(block.Txs)
	block.Header.TxCount = uint32(txCount)

	// TxRoot/RwSetRoot
	var err error
	txHashes := make([][]byte, txCount)
	startT := time.Now()
	for i, tx := range block.Txs {
		// finalize tx, put rwsethash into tx.Result
		rwSet := txRWSetMap[tx.Payload.TxId]
		if rwSet == nil {
			rwSet = &commonpb.TxRWSet{
				TxId:     tx.Payload.TxId,
				TxReads:  nil,
				TxWrites: nil,
			}
		}
		var rwSetHash []byte
		//hashETime := time.Now()
		rwSetHash, err = utils.CalcRWSetHash(hashType, rwSet)
		if err != nil {
			return err
		}
		//eHashTime := time.Now()
		if tx.Result == nil {
			// in case tx.Result is nil, avoid panic
			e := fmt.Errorf("tx(%s) result == nil", tx.Payload.TxId)
			//logger.Error(e.Error())
			return e
		}
		tx.Result.RwSetHash = rwSetHash
		// calculate complete tx hash, include tx.Header, tx.Payload, tx.Result
		txHash, err := utils.CalcTxHash(hashType, tx, logger)
		if err != nil {
			return err
		}
		txHashes[i] = txHash
		//logger.Debugf("FinalizeBlockTx spend time tx[%d]ns, ,TxHash[%+v]",eHashTime.Sub(hashETime).Nanoseconds(),time.Since(eHashTime))
	}
	end := time.Now()
	block.Header.TxRoot, err = hash.GetMerkleRoot(hashType, txHashes)
	endM := time.Now()
	if err != nil {
		//logger.Warnf("get tx merkle root error %s", err)
		return err
	}
	block.Header.RwSetRoot, err = utils.CalcRWSetRoot(hashType, block.Txs)
	endR := time.Now()
	if err != nil {
		//logger.Warnf("get rwset merkle root error %s", err)
		return err
	}

	// DagDigest
	dagHash, err := utils.CalcDagHash(hashType, block.Dag)
	if err != nil {
		//logger.Warnf("get dag hash error %s", err)
		return err
	}
	block.Header.DagHash = dagHash
	logger.Debugf("FinalizeBlock spend time tx[%d]ms, MerkleRoot[%d]ns.RWRoot[%d]ns ,DagHash[%+v]", end.Sub(startT).Milliseconds(),
		endM.Sub(end).Nanoseconds(), endR.Sub(endM).Nanoseconds(), time.Since(endR))
	return nil
}
