/**
 * @Author: starxxliu
 * @Date: 2021/11/19 6:46 下午
 */

package scheduler

import (
	"sort"
	"sync"

	"chainmaker.org/chainmaker/common/v2/bitmap"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"go.uber.org/zap"
)

type SnapshotImpl struct {
	lock         sync.Mutex
	txRWSetTable []*commonPb.TxRWSet
	txTable      []*commonPb.Transaction
	blockHeight  uint64
	log          *zap.SugaredLogger
}

func NewSnapshotImpl(blockHeight uint64, log *zap.SugaredLogger) *SnapshotImpl {

	return &SnapshotImpl{
		lock:        sync.Mutex{},
		blockHeight: blockHeight,
		log:         log,
	}
}

// SetTxRWSet return current transaction read write set
func (s *SnapshotImpl) SetTxRWSet(tx *commonPb.Transaction, txReadKeyMap map[string]*commonPb.TxRead, txWriteKeyMap map[string]*commonPb.TxWrite, runVmSuccess bool) {

	txRWSet := &commonPb.TxRWSet{
		TxId:     tx.Payload.TxId,
		TxReads:  nil,
		TxWrites: nil,
	}

	// read set
	{
		txIds := make([]string, 0, len(txReadKeyMap))
		for txId := range txReadKeyMap {
			txIds = append(txIds, txId)
		}
		sort.Strings(txIds)
		for _, k := range txIds {
			txRWSet.TxReads = append(txRWSet.TxReads, txReadKeyMap[k])
		}
	}

	// write set
	if runVmSuccess {
		txIds := make([]string, 0, len(txWriteKeyMap))
		for txId := range txWriteKeyMap { //这里是命名不标准，其实是key
			txIds = append(txIds, txId)
		}
		sort.Strings(txIds)
		for _, k := range txIds {
			txRWSet.TxWrites = append(txRWSet.TxWrites, txWriteKeyMap[k])
		}
		// sql nil key tx writes
		//txRWSet.TxWrites = append(txRWSet.TxWrites, txWriteKeySql...)
	} else {
		// ddl sql tx writes
		//txRWSet.TxWrites = txWriteKeyDdlSql
	}
	s.txRWSetTable = append(s.txRWSetTable, txRWSet)

	// Add to transaction table
	s.txTable = append(s.txTable, tx)

	return
}

func (s *SnapshotImpl) GetTxRWSetTable() []*commonPb.TxRWSet {
	return s.txRWSetTable
}

func (s *SnapshotImpl) BuildDAG(isSql bool) *commonPb.DAG {

	s.lock.Lock()
	defer s.lock.Unlock()

	txCount := len(s.txTable)
	//log.Debugf("start building DAG for block %d with %d txs", s.blockHeight, txCount)
	dag := &commonPb.DAG{}
	if txCount == 0 {
		return dag
	}

	// build read-write bitmap for all transactions
	readBitmaps, writeBitmaps := s.buildRWBitmaps()
	cumulativeReadBitmap, cumulativeWriteBitmap := s.buildCumulativeBitmap(readBitmaps, writeBitmaps)

	dag.Vertexes = make([]*commonPb.DAG_Neighbor, txCount)

	// build DAG base on read and write bitmaps
	// reachMap describes reachability from tx i to tx j in DAG.
	// For example, if the DAG is tx3 -> tx2 -> tx1 -> begin, the reachMap is
	// 		tx1		tx2		tx3
	// tx1	0		0		0
	// tx2	1		0		0
	// tx3	1		1		0
	reachMap := make([]*bitmap.Bitmap, txCount)
	if isSql {
		for i := 0; i < txCount; i++ {
			dag.Vertexes[i] = &commonPb.DAG_Neighbor{
				Neighbors: make([]uint32, 0, 1),
			}
			if i != 0 {
				dag.Vertexes[i].Neighbors = append(dag.Vertexes[i].Neighbors, uint32(i-1))
			}
		}
	} else {
		for i := 0; i < txCount; i++ {
			// 1、get read and write bitmap for tx i
			readBitmapForI := readBitmaps[i]
			writeBitmapForI := writeBitmaps[i]

			// directReach is used to build DAG
			// reach is used to save reachability we have already known
			directReachFromI := &bitmap.Bitmap{}
			reachFromI := &bitmap.Bitmap{}
			reachFromI.Set(i)

			if i > 0 && s.fastConflicted(readBitmapForI, writeBitmapForI, cumulativeReadBitmap[i-1], cumulativeWriteBitmap[i-1]) {
				// check reachability one by one, then build table
				s.buildReach(i, reachFromI, readBitmaps, writeBitmaps, readBitmapForI, writeBitmapForI, directReachFromI, reachMap)
			}
			reachMap[i] = reachFromI

			// build DAG based on directReach bitmap
			dag.Vertexes[i] = &commonPb.DAG_Neighbor{
				Neighbors: make([]uint32, 0, 16),
			}
			for _, j := range directReachFromI.Pos1() {
				dag.Vertexes[i].Neighbors = append(dag.Vertexes[i].Neighbors, uint32(j))
			}
		}
	}
	//log.Debugf("build DAG for block %d finished", s.blockHeight)
	return dag
}

// Build txs' read bitmap and write bitmap, so we can use AND to simplify read/write set conflict detection process.
// keyDict: key string -> key index in bitmap, e.g., key1 -> 0, key2 -> 1, key3 -> 2
// read/write Table:	tx1: {key1->value1, key3->value3}; tx2: {key2->value2, key3->value4}
// read/write bitmap: 			key1	key2	key3
//						tx1		1		0		1
// 						tx2		0		1		1
func (s *SnapshotImpl) buildRWBitmaps() ([]*bitmap.Bitmap, []*bitmap.Bitmap) {
	dictIndex := 0
	txCount := len(s.txTable)
	readBitmap := make([]*bitmap.Bitmap, txCount)
	writeBitmap := make([]*bitmap.Bitmap, txCount)
	keyDict := make(map[string]int, 1024)
	for i := 0; i < txCount; i++ {
		readTableItemForI := s.txRWSetTable[i].TxReads
		writeTableItemForI := s.txRWSetTable[i].TxWrites

		readBitmap[i] = &bitmap.Bitmap{}
		for _, keyForI := range readTableItemForI {
			if existIndex, ok := keyDict[string(keyForI.Key)]; !ok {
				keyDict[string(keyForI.Key)] = dictIndex
				readBitmap[i].Set(dictIndex)
				dictIndex++
			} else {
				readBitmap[i].Set(existIndex)
			}
		}

		writeBitmap[i] = &bitmap.Bitmap{}
		for _, keyForI := range writeTableItemForI {
			if existIndex, ok := keyDict[string(keyForI.Key)]; !ok {
				keyDict[string(keyForI.Key)] = dictIndex
				writeBitmap[i].Set(dictIndex)
				dictIndex++
			} else {
				writeBitmap[i].Set(existIndex)
			}
		}
	}
	return readBitmap, writeBitmap
}

func (s *SnapshotImpl) GetTxTable() []*commonPb.Transaction {
	return s.txTable
}

func (s *SnapshotImpl) buildCumulativeBitmap(readBitmap []*bitmap.Bitmap, writeBitmap []*bitmap.Bitmap) ([]*bitmap.Bitmap, []*bitmap.Bitmap) {
	cumulativeReadBitmap := make([]*bitmap.Bitmap, len(readBitmap))
	cumulativeWriteBitmap := make([]*bitmap.Bitmap, len(writeBitmap))

	for i, b := range readBitmap {
		cumulativeReadBitmap[i] = b.Clone()
		if i > 0 {
			cumulativeReadBitmap[i].Or(cumulativeReadBitmap[i-1])
		}
	}
	for i, b := range writeBitmap {
		cumulativeWriteBitmap[i] = b.Clone()
		if i > 0 {
			cumulativeWriteBitmap[i].Or(cumulativeWriteBitmap[i-1])
		}
	}
	return cumulativeReadBitmap, cumulativeWriteBitmap
}

// check reachability one by one, then build table
func (s *SnapshotImpl) buildReach(i int, reachFromI *bitmap.Bitmap,
	readBitmaps []*bitmap.Bitmap, writeBitmaps []*bitmap.Bitmap,
	readBitmapForI *bitmap.Bitmap, writeBitmapForI *bitmap.Bitmap,
	directReachFromI *bitmap.Bitmap, reachMap []*bitmap.Bitmap) {

	for j := i - 1; j >= 0; j-- {
		if reachFromI.Has(j) {
			continue
		}

		readBitmapForJ := readBitmaps[j]
		writeBitmapForJ := writeBitmaps[j]
		if s.conflicted(readBitmapForI, writeBitmapForI, readBitmapForJ, writeBitmapForJ) {
			directReachFromI.Set(j)
			reachFromI.Or(reachMap[j])
		}
	}
}

// Conflict cases: I read & J write; I write & J read; I write & J write
func (s *SnapshotImpl) conflicted(readBitmapForI, writeBitmapForI, readBitmapForJ, writeBitmapForJ *bitmap.Bitmap) bool {
	if readBitmapForI.InterExist(writeBitmapForJ) || writeBitmapForI.InterExist(writeBitmapForJ) || writeBitmapForI.InterExist(readBitmapForJ) {
		return true
	}
	return false
}

// fast conflict cases: I read & J write; I write & J read; I write & J write
func (s *SnapshotImpl) fastConflicted(readBitmapForI, writeBitmapForI, cumulativeReadBitmap, cumulativeWriteBitmap *bitmap.Bitmap) bool {
	if readBitmapForI.InterExist(cumulativeWriteBitmap) || writeBitmapForI.InterExist(cumulativeWriteBitmap) || writeBitmapForI.InterExist(cumulativeReadBitmap) {
		return true
	}
	return false
}
