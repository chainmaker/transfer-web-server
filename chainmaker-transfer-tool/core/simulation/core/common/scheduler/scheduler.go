/**
 * @Author: starxxliu
 * @Date: 2021/11/11 2:15 下午
 */

package scheduler

import (
	"errors"
	"fmt"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/transfer-tool/common"
	"chainmaker.org/chainmaker/transfer-tool/core/simulation/core/native"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	"go.uber.org/zap"
)

const (
	DefaultStateLen          = 64 // key & name for contract state length
	ParametersValueMaxLength = 1024 * 1024
)

// TxScheduler transaction scheduler structure
type TxScheduler struct {
	log *zap.SugaredLogger
	Sim provider.SimContractExec
}

func NewTxScheduler(log *zap.SugaredLogger, sim provider.SimContractExec) *TxScheduler {
	return &TxScheduler{
		log: log,
		Sim: sim,
	}
}

// Schedule according to a batch of transactions, and generating DAG according to the conflict relationship
func (ts *TxScheduler) Schedule(block *commonpb.Block, txBatch []*commonpb.Transaction, hashType string, tblock *provider.TempBlock) (map[string]*commonpb.TxRWSet,
	map[string][]*commonpb.ContractEvent, error) {
	startT := time.Now()
	ts.log.Debugf("start schedule new block height[%d] tx num[%d] ", block.Header.BlockHeight, len(txBatch))
	txRWSetMap := make(map[string]*commonpb.TxRWSet)
	snapshot := NewSnapshotImpl(block.Header.BlockHeight, ts.log)

	for _, tx := range txBatch {
		txResult := &commonpb.Result{
			Code: commonpb.TxStatusCode_SUCCESS,
			ContractResult: &commonpb.ContractResult{
				Code:    uint32(0),
				Result:  nil,
				Message: "OK",
			},
			RwSetHash: nil,
		}
		tx.Result = txResult
		parseT := time.Now()
		parameters, err := ts.parseParameter(tx.Payload.Parameters)
		if err != nil {
			return nil, nil, err
		}

		ts.log.Debugf("parseParameter spend time[%d]us ", time.Now().Sub(parseT).Microseconds())
		txWriteKeyMap := make(map[string]*commonpb.TxWrite, 0)
		txReadKeyMap := make(map[string]*commonpb.TxRead, 0)
		ntxWriteKeyMap := make(map[string]*commonpb.TxWrite, 0)

		if IsContractManageNative(tx.Payload.ContractName, tx.Payload.TxType) { //进行合约创建时的读写集写入,以及短证书交易
			if IsCertManageContract(tx.Payload.ContractName) {

				txWriteKeyMap, txResult.ContractResult.Result, err = native.SimAddShutCert(tx, parameters, hashType)
				if err != nil {
					return nil, nil, err
				}
				//txReadKeyMap, ntxWriteKeyMap, err = native.FetchDisabledContractList(true)

			} else if IsContractManageContract(tx.Payload.ContractName) {
				var name string
				txWriteKeyMap, name, err = ts.simCreateContract(tx, parameters, hashType)
				if err != nil {
					return nil, nil, err
				}

				isCertHash := false
				if tx.Sender.Signer.MemberType == 1 {
					isCertHash = true
				}

				txReadKeyMap, ntxWriteKeyMap, err = native.InstallContractReader(tx.Sender.Signer.MemberInfo, nil, name, isCertHash)
			}

		} else { //普通交易
			startT := time.Now()
			err = ts.Sim.CreateTxWriteKeyMap(txWriteKeyMap, parameters, tblock.OBlock)
			//err = native.CreateTxWriteKeyMap(txWriteKeyMap, parameters, fblock.Envs[index].Payload.Transaction.ChaincodeAction.Response.TxRwSet)
			//txReadKeyMap = native.CreateSenderReader(tx.Sender.Signer.MemberInfo, nil)
			ts.log.Debugf("CreateTxWriteKeyMap spend time[%+v]", time.Since(startT))

			if tx.Payload.ContractName == syscontract.SystemContract_name[int32(syscontract.SystemContract_T)] {
				txResult.ContractResult.Message = "OK"
			} else {
				txResult.ContractResult.Message = "Success"
			}

		}

		if err != nil {
			return nil, nil, err
		}

		for key, write := range ntxWriteKeyMap {
			//TODO 等待确定key 值最大长度，增加一层过滤
			txWriteKeyMap[key] = write
		}
		//merage writeSet
		snapshot.SetTxRWSet(tx, txReadKeyMap, txWriteKeyMap, true)
	}

	block.Dag = snapshot.BuildDAG(false)
	ts.log.Debugf("BuildDAG speed time[%d]us", time.Now().Sub(startT).Microseconds())
	block.Txs = snapshot.GetTxTable()

	txRWSetTable := snapshot.GetTxRWSetTable()
	for _, txRWSet := range txRWSetTable {
		if txRWSet != nil {
			txRWSetMap[txRWSet.TxId] = txRWSet
		}
	}
	ts.log.Debugf("end schedule new block height[%d] spend time[%+v]", block.Header.BlockHeight, time.Since(startT))
	return txRWSetMap, nil, nil
}

func (ts *TxScheduler) parseParameter(parameterPairs []*commonpb.KeyValuePair) (map[string][]byte, error) {
	// verify parameters
	if len(parameterPairs) > protocol.ParametersKeyMaxCount {
		return nil, fmt.Errorf("expect parameters length less than %d, but got %d", protocol.ParametersKeyMaxCount, len(parameterPairs))
	}
	parameters := make(map[string][]byte, 16)
	for i := 0; i < len(parameterPairs); i++ {
		key := parameterPairs[i].Key
		value := parameterPairs[i].Value
		if len(key) > DefaultStateLen {
			return nil, fmt.Errorf("expect key length less than %d, but got %d", DefaultStateLen, len(key))
		}
		// 这部分内容提前放到构建模块去验证
		//match, err := regexp.MatchString(protocol.DefaultStateRegex, key)
		//if err != nil || !match {
		//	return nil, fmt.Errorf(
		//		"expect key no special characters, but got key:[%s]. letter, number, dot and underline are allowed",
		//		key)
		//}
		if len(value) > 100*ParametersValueMaxLength { //TODO
			return nil, fmt.Errorf("expect value length less than %d, but got %d", protocol.ParametersValueMaxLength, len(value))
		}

		parameters[key] = value
	}
	return parameters, nil
}

func (ts *TxScheduler) simCreateContract(tx *commonpb.Transaction, parameters map[string][]byte, hashType string) (map[string]*commonpb.TxWrite, string, error) {
	name := string(parameters[syscontract.InitContract_CONTRACT_NAME.String()])

	version := string(parameters[syscontract.InitContract_CONTRACT_VERSION.String()])

	byteCode := parameters[syscontract.InitContract_CONTRACT_BYTECODE.String()]

	runtime := parameters[syscontract.InitContract_CONTRACT_RUNTIME_TYPE.String()]
	if utils.IsAnyBlank(name, version, byteCode, runtime) {
		return nil, "", errors.New("params contractName/version/byteCode/runtimeType cannot be empty")
	}

	runtimeInt := commonpb.RuntimeType_value[string(runtime)]
	if runtimeInt == 0 || int(runtimeInt) >= len(commonpb.RuntimeType_value) {
		return nil, "", errors.New("params runtimeType[" + string(runtime) + "] is error")
	}
	runtimeType := commonpb.RuntimeType(runtimeInt)

	wSet, result, err := ts.SimInstallContract(tx, name, version, byteCode, runtimeType, hashType, parameters)

	if err == nil {
		tx.Result.ContractResult.Result = result
	}
	return wSet, name, err
}

func (ts *TxScheduler) SimInstallContract(tx *commonpb.Transaction, name, version string, byteCode []byte,
	runTime commonpb.RuntimeType, hashType string, initParameters map[string][]byte) (map[string]*commonpb.TxWrite, []byte, error) {
	if !utils.CheckContractNameFormat(name) {
		return nil, nil, errors.New("invalid contract name")
	}
	txWriteKeyMap := make(map[string]*commonpb.TxWrite, 0) //后期会进行相应的排序

	key := utils.GetContractDbKey(name)

	signer := tx.Sender.GetSigner()

	member, err := common.NewMemberFromParam(signer, protocol.RoleAdmin, hashType)
	if err != nil {
		return nil, nil, err
	}

	creator := &accesscontrol.MemberFull{
		OrgId:      signer.OrgId,
		MemberType: signer.MemberType,
		MemberInfo: signer.MemberInfo,
		MemberId:   member.GetMemberId(),
		Role:       string(member.GetRole()),
		Uid:        member.GetUid(),
	}

	contract := &commonpb.Contract{
		Name:        name,
		Version:     version,
		RuntimeType: runTime,
		Status:      commonpb.ContractStatus_NORMAL,
		Creator:     creator,
	}
	cdata, _ := contract.Marshal()

	txWriteKeyMap[native.ConstructKey(native.InstallContractName, key)] = &commonpb.TxWrite{
		Key:          key,
		Value:        cdata,
		ContractName: native.InstallContractName,
	}

	byteCodeKey := utils.GetContractByteCodeDbKey(name)
	txWriteKeyMap[native.ConstructKey(native.InstallContractName, byteCodeKey)] = &commonpb.TxWrite{
		Key:          byteCodeKey,
		Value:        byteCode,
		ContractName: native.InstallContractName,
	}

	err = ts.Sim.CreateWriteKeyMap(txWriteKeyMap, initParameters)
	if err != nil {
		return nil, nil, err
	}

	contractByte, _ := contract.Marshal()
	return txWriteKeyMap, contractByte, nil
}

func IsContractManageNative(contractName string, txType commonpb.TxType) bool {
	return IsNativeContract(contractName) && IsNativeTxType(txType)
}

// IsNativeContract return is native contract name
func IsNativeContract(contractName string) bool {
	return syscontract.SystemContract_name[int32(syscontract.SystemContract_CONTRACT_MANAGE)] == contractName ||
		syscontract.SystemContract_name[int32(syscontract.SystemContract_CERT_MANAGE)] == contractName
}

func IsCertManageContract(contractName string) bool {
	return syscontract.SystemContract_name[int32(syscontract.SystemContract_CERT_MANAGE)] == contractName
}

func IsContractManageContract(contractName string) bool {
	return syscontract.SystemContract_name[int32(syscontract.SystemContract_CONTRACT_MANAGE)] == contractName
}

//TODO: Devin: Remove it
// IsNativeTxType return is native contract supported transaction type
func IsNativeTxType(txType commonpb.TxType) bool {
	switch txType {
	case commonpb.TxType_QUERY_CONTRACT,
		commonpb.TxType_INVOKE_CONTRACT:
		//commonPb.TxType_INVOKE_CONTRACT:
		return true
	default:
		return false
	}
}

func constructKey(contractName string, key []byte) string {
	return contractName + string(key)
}
