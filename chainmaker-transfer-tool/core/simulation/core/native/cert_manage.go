/**
 * @Author: starxxliu
 * @Date: 2021/11/18 9:18 下午
 */

package native

import (
	"fmt"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/transfer-tool/utils"
)

const (
	PrefixContractInfo    = "Contract:"
	constructKeySeparator = "#"
)

func SimAddShutCert(tx *commonPb.Transaction, params map[string][]byte, hashType string) (map[string]*commonPb.TxWrite, []byte, error) {
	sender := tx.Sender
	memberInfo := sender.Signer.GetMemberInfo()

	txWriteKeyMap := make(map[string]*commonPb.TxWrite, 0)

	certHash, err := utils.GetCertificateIdHex(memberInfo, hashType)
	if err != nil {
		return nil, nil, fmt.Errorf("get certHash failed, err: %s", err.Error())
	}

	contractName := syscontract.SystemContract_CERT_MANAGE.String()
	key := []byte(certHash)
	value := memberInfo

	txWriteKeyMap[ConstructKey(contractName, key)] = &commonPb.TxWrite{
		Key:          key,
		Value:        value,
		ContractName: contractName,
	}

	return txWriteKeyMap, key, nil

}

func ConstructKey(contractName string, key []byte) string {
	return contractName + constructKeySeparator + string(key)
}

func GetContractDbKey(contractName string) []byte {
	return []byte(PrefixContractInfo + contractName)
}
