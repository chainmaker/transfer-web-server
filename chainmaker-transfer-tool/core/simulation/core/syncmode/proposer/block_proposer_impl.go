/**
 * @Author: starxxliu
 * @Date: 2021/11/10 8:21 下午
 */

package proposer

import (
	"context"
	"encoding/hex"
	"time"

	"chainmaker.org/chainmaker/transfer-tool/provider"

	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/transfer-tool/common/msgbus"
	"chainmaker.org/chainmaker/transfer-tool/config"
	tbftblock "chainmaker.org/chainmaker/transfer-tool/core/simulation/consensus"
	"chainmaker.org/chainmaker/transfer-tool/core/simulation/core/common"
	"go.uber.org/zap"
)

// BlockProposerImpl implements BlockProposer interface.
// In charge of propose a new block.
type BlockProposerImpl struct {
	blockBuilder *common.BlockBuilder
	tbft         *tbftblock.TbftBlock
	msgBus       msgbus.MessageBus // channel to give out proposed block
	logger       *zap.SugaredLogger
	signChan     chan *provider.TempBlock
}

func NewBlockProposerImpl(chainId, Id, hashType string, identity protocol.SigningMember, log *zap.SugaredLogger,
	bus msgbus.MessageBus, signChan chan *provider.TempBlock, sim provider.SimContractExec) *BlockProposerImpl {

	path := config.TransferConfig.Chain.PrivKeyFilePath
	tbft := tbftblock.NewTbftBlock(chainId, Id, hashType, path, identity, bus, signChan)

	builder := common.NewBlockBuilder(chainId, hashType, identity, sim, log)

	proposer := &BlockProposerImpl{
		blockBuilder: builder,
		tbft:         tbft,
		msgBus:       bus,
		logger:       log,
		signChan:     signChan,
	}
	return proposer
}

// Proposing propose a block in new height
func (bp *BlockProposerImpl) Proposing(height uint64, preHash []byte, temp *provider.TempBlock) ([]byte, *consensuspb.ProposalBlock, error) {
	bp.logger.Debugf("core engine start proposing chainmaker block[%d],prehash[%s]", height, hex.EncodeToString(preHash))
	startT := time.Now()
	block, rwSetMap, _, _, err := bp.blockBuilder.GenerateNewBlock(height, preHash, temp.TxBatch, temp)
	if err != nil {
		return nil, nil, err
	}

	proposal := &consensuspb.ProposalBlock{Block: block, TxsRwSet: rwSetMap}
	//fBlock, err := bp.tbft.HandleProposedBlock(proposal)
	//if err != nil {
	//	return nil, nil, err
	//}
	//proposal.Block = fBlock

	bp.logger.Debugf("core engine end proposing chainmaker block height [%d] speed time[%+v]", height, time.Since(startT))
	return nil, proposal, nil
}

func (bp *BlockProposerImpl) SendMes(err error, done bool) {
	proposal := &provider.TempBlock{0, nil, nil, nil, nil, done, err, ""}
	//bp.msgBus.PublishSafe(msgbus.CommitBlock, proposal)
	bp.signChan <- proposal
}

func (bp *BlockProposerImpl) SetPreHash(hash []byte) {
	bp.tbft.SetPreHash(hash)
}

func (bp *BlockProposerImpl) GetPreHash() []byte {
	return bp.tbft.GetPreHash()
}

func (bp *BlockProposerImpl) StartConsensus(ctx context.Context) {
	bp.tbft.Start(ctx)
}

func (bp *BlockProposerImpl) HandleProposedBlock(proposedBlock *consensuspb.ProposalBlock) (*commonpb.Block, error) {
	return bp.tbft.HandleProposedBlock(proposedBlock)
}
