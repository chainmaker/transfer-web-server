/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package proposer

import (
	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/transfer-tool/provider"
)

func (bp *BlockProposerImpl) generateNewBlock(proposingHeight uint64, preHash []byte, txBatch []*commonpb.Transaction,
	fblock *provider.TempBlock) (*commonpb.Block, map[string]*commonpb.TxRWSet, map[string][]*commonpb.ContractEvent, []int64, error) {
	return bp.blockBuilder.GenerateNewBlock(proposingHeight, preHash, txBatch, fblock)
}
