package config

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestInitConfig(t *testing.T) {
	confPath := "../config_path"
	env := "dev"
	InitConfig(confPath, env)
}

func TestWriteConfig(t *testing.T) {
	TransferConfig = &Config{}

	configByte, err := json.Marshal(TransferConfig)
	require.Nil(t, err)

	configMap := make(map[string]interface{})
	require.Nil(t, json.Unmarshal(configByte, configMap))

	println("%+v", configMap)
}
