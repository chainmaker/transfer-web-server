/**
 * @Author: jasonruan
 * @Date:   2020-12-09 18:18:21
 **/

package config

import (
	"chainmaker.org/chainmaker/store/v2/conf"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/hokaccha/go-prettyjson"
	"github.com/spf13/viper"
)

var TransferViper *viper.Viper
var TransferConfig *Config

var (
	gEnv        string
	gConfPath   string
	ConfEnvPath string
	WhiteIp     = "white_ip"
	dev         = "dev"
	prod        = "prod"
)

// GetConfigEnv - 获取配置环境
func GetConfigEnv() string {
	var env string

	n := len(os.Args)
	for i := 1; i < n-1; i++ {
		if os.Args[i] == "-e" || os.Args[i] == "--env" {
			env = os.Args[i+1]
			break
		}
	}
	fmt.Println("[env]:", env)

	if env == "" {
		fmt.Println("env is empty, set default: dev")
		env = dev
	}

	if env == "" || (env != dev && env != "test" && env != prod) {
		fmt.Println(env + " is invalid")
		os.Exit(1)
	}

	return env
}

func InitConfig(confPath, env string) {
	gEnv = env
	gConfPath = confPath
	if gConfPath == "" {
		gConfPath = "../config"
	}

	var err error
	if TransferViper, err = initCMViper(env); err != nil {
		log.Fatal("Load config failed, ", err)
	}

	TransferConfig = &Config{}
	if err = TransferViper.Unmarshal(&TransferConfig); err != nil {
		log.Fatal("Unmarshal config failed, ", err)
	}

	//MiddlewareConfig.printLog(env)
}

func initCMViper(env string) (*viper.Viper, error) {
	//if env == "" {
	//	env = dev
	//}

	cmViper := viper.New()

	ConfEnvPath = filepath.Join(gConfPath, gEnv)

	//cmViper.SetConfigFile(ConfEnvPath + "/" + "log.yml")
	//if err := cmViper.ReadInConfig(); err != nil {
	//	//if env != "prod" {
	//	//	fmt.Printf("WARN: in [%s] can use default config, ignore err: %s\n", env, err)
	//	//	return cmViper, nil
	//	//}
	//	return nil, err
	//}

	cmViper.SetConfigFile(ConfEnvPath + "/" + "config.yml")
	if err := cmViper.MergeInConfig(); err != nil {
		return nil, err
	}

	//国密新增参数
	max := cmViper.Get("common.tencentsm.ctx_pool_size.max")
	viper.Set("common.tencentsm.ctx_pool_size.max", max)
	init := cmViper.Get("common.tencentsm.ctx_pool_size.init")
	viper.Set("common.tencentsm.ctx_pool_size.init", init)

	return cmViper, nil
}

func (c *Config) printLog(env string) {
	if env == "prod" {
		return
	}

	json, err := prettyjson.Marshal(c)
	if err != nil {
		log.Fatalf("marshal alarm config failed, %s", err.Error())
	}

	fmt.Println(string(json))
}

func NewStorageConfig(cfg map[string]interface{}) (*conf.StorageConfig, error)  {
	return conf.NewStorageConfig(cfg)
}