/**
 * @Author: starxxliu
 * @Date: 2022/3/28 5:31 下午
 */

package fabric

import (
	"fmt"

	"chainmaker.org/chainmaker/transfer-tool/config"
	fabric14 "chainmaker.org/chainmaker/transfer-tool/fabric/fabric_1.4"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"go.uber.org/zap"
)

//包含原链拉取block，解析block
func NewFetcher(version string, opts ...provider.CreateFetcherConfig) (provider.Fetcher, error) {

	config := &provider.FetcherConfig{}
	for _, opt := range opts {
		opt(config)
	}

	switch version {
	case "1.4":
		return fabric14.NewCreator(config.Height, config.Pre, config.ChainId, config.UserName, config.Path, config.Send, config.Logger)
	default:
		return nil, fmt.Errorf("non-existent fabric version")
	}
}

//构建目标链的交易batch
func NewTxsCreator(config *config.Config, log *zap.SugaredLogger, version string, certHash []byte, db provider.TransferDB) (provider.ChainMakerTxBatch, error) {
	switch version {
	case "1.4":
		return fabric14.NewTxsCreator(config, log, certHash, db)
	default:
		return nil, fmt.Errorf("non-existent fabric version")
	}
}

func NewChecker(version string, contractList map[string]uint64) (provider.Checker, error) {
	switch version {
	case "1.4":
		return fabric14.NewChecker(contractList)
	default:
		return nil, fmt.Errorf("non-existent fabric version")
	}
}

//NewSimContract 模拟合约执行
func NewSimContract(version string) (provider.SimContractExec, error) {
	switch version {
	case "1.4":
		return fabric14.NewSimContract()
	default:
		return nil, fmt.Errorf("non-existent fabric version")
	}
}
