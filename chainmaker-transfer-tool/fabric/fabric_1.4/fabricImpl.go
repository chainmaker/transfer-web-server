/**
 * @Author: starxxliu
 * @Date: 2022/3/28 5:57 下午
 */

package fabric_1_4

import (
	"context"
	"fmt"

	commonv2 "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/fabric/fabric_1.4/chainmaker_sdk"
	"chainmaker.org/chainmaker/transfer-tool/fabric/fabric_1.4/checker"
	"chainmaker.org/chainmaker/transfer-tool/fabric/fabric_1.4/fabric"
	"chainmaker.org/chainmaker/transfer-tool/fabric/fabric_1.4/sim_contract"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"github.com/hyperledger/fabric-protos-go/common"
	"go.uber.org/zap"
)

type Creator struct {
	fetch *fabric.Fetcher
}

func (c *Creator) Start(ctx context.Context) error {
	return c.fetch.Start(ctx)
}

func (c *Creator) IsFinish(h uint64) bool {
	return c.fetch.IsFinish(h)
}

func (c *Creator) QueryBlockByHeight(height uint64) (interface{}, error) {
	return c.fetch.QueryBlockByHeight(height)
}

func (c *Creator) GetLastBlockHeight() (uint64, error) {
	return c.fetch.GetLastBlockHeight()
}

func (c *Creator) Close() {
	c.fetch.Close()
}

func (c *Creator) CloseSdk() {
	c.fetch.CloseSdk()
}

//UpdateState update state
func (c *Creator) UpdateState() {
	c.fetch.UpdateState()
}

func (c *Creator) NextRound() {
	c.fetch.NextRound()
}

func (c *Creator) GetThresholdRound() int {
	return fabric.ThresholdRound
}

func (c *Creator) GetRoundNum() int {
	return fabric.RoundNum
}

func (c *Creator) ParseBlock(block interface{}) (interface{}, error) {
	b, ok := block.(*common.Block)
	if ok {
		return fabric.ParseBlock(b)
	}

	return nil, fmt.Errorf("not the expect block type")
}

func NewCreator(height uint64, pre []byte, chainId, userName, path string, send chan *provider.SendInfo,
	logger *zap.SugaredLogger) (provider.Fetcher, error) {

	fetch, err := fabric.NewFetcher(height, pre, chainId, userName, path, send, logger)
	if err != nil {
		return nil, err
	}

	return &Creator{fetch}, nil
}

type TxsCreator struct {
	txBatch *chainmaker_sdk.TxBatchImpl
}

func NewTxsCreator(config *config.Config, log *zap.SugaredLogger, certHash []byte, db provider.TransferDB) (provider.ChainMakerTxBatch, error) {
	txBatch, err := chainmaker_sdk.NewTxBatchImpl(config, log, certHash, db)

	return &TxsCreator{
		txBatch: txBatch,
	}, err
}

func (t *TxsCreator) CreateTxBatch(block interface{}) ([]*commonv2.Transaction, error) {
	b, ok := block.(*fabric.BlockInfo)
	if ok {
		return t.txBatch.CreateTxBatch(b)
	}

	return nil, fmt.Errorf("not the expect block type")
}

func (t *TxsCreator) CreateFirstTxBatch(blocks []interface{}) ([]*commonv2.Transaction, error) {
	bs := make([]*common.Block, 0)
	for _, block := range blocks {
		b, ok := block.(*common.Block)
		if !ok {
			return nil, fmt.Errorf("not the expect block type,ecpect type *common.Block")
		}
		bs = append(bs, b)
	}

	return t.txBatch.CreateFirstTxBatch(bs)
}

func (t *TxsCreator) GetUserCrtByte() []byte {
	return t.txBatch.GetUserCrtByte()
}

func (t *TxsCreator) GetChainMakerClientUserCrtHash() []byte {
	return t.txBatch.GetClientUserCrtHash()
}

type BlockCheck struct {
}

func (check *BlockCheck) CheckBlock(info interface{}) ([]string, error) {
	block, ok := info.(*provider.TempBlock)
	if !ok {
		return nil, fmt.Errorf("not the expect block type. expect tempBlock type")
	}
	return checker.CheckBlock(block)
}

func NewChecker(contractList map[string]uint64) (provider.Checker, error) {
	checker.ContractList = contractList
	return &BlockCheck{}, nil
}

type SimContract struct {
}

func (sim *SimContract) CreateWriteKeyMap(txWriteKeyMap map[string]*commonv2.TxWrite, wSet map[string][]byte) error {

	return sim_contract.CreateWriteKeyMap(txWriteKeyMap, wSet)
}

func (sim *SimContract) CreateTxWriteKeyMap(txWriteKeyMap map[string]*commonv2.TxWrite, wSet map[string][]byte, txRwSet interface{}) error {
	//rwSet, ok := txRwSet.(*rwsetutil.TxRwSet)
	//if !ok {
	//	return fmt.Errorf("expect txRwSet type is *rwsetutil.TxRwSet ")
	//}
	return sim_contract.CreateTxWriteKeyMap(txWriteKeyMap, wSet, nil)
}

func (sim *SimContract) GetOriginChainTimeStamp(block interface{}) (int64, error) {

	return 0, nil
}

func NewSimContract() (provider.SimContractExec, error) {
	return &SimContract{}, nil
}
