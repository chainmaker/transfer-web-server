/**
 * @Author: starxxliu
 * @Date: 2022/4/1 7:56 下午
 */

package fabric

import (
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

//BlockInfo
type BlockInfo struct {
	Block            *common.Block
	Envs             []*Envelope
	TxValidationCode []peer.TxValidationCode
	IsConfig         bool
}

// Envelope clean struct for envelope
type Envelope struct {
	Payload struct {
		Header struct {
			ChannelHeader *common.ChannelHeader
			//SignatureHeader *common.SignatureHeader
		}
		Transaction struct {
			//Header          *common.SignatureHeader
			ChaincodeAction struct {
				Proposal struct {
					Input     *peer.ChaincodeSpec
					InputByte []byte
				}
				Response struct {
					ProposalHash    []byte
					ChaincodeAction *peer.ChaincodeAction
					//ResponseByte    []byte
					TxRwSet *rwsetutil.TxRwSet
				}
				//Endorses []*peer.Endorsement
			}
		}
	}
	Signature []byte
}
