/**
 * @Author: starxxliu
 * @Date: 2021/11/16 10:00 上午
 */

package fabric

import (
	"context"
	"errors"
	"fmt"
	"sync/atomic"
	"time"

	"chainmaker.org/chainmaker/transfer-tool/provider"

	"chainmaker.org/chainmaker/transfer-tool/common/transfer"
	configL "chainmaker.org/chainmaker/transfer-tool/config"
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/panjf2000/ants/v2"
	"go.uber.org/zap"
)

var (
	retryNum       = 10
	RoundNum       = 1000
	ThresholdRound = 500 //轮次阀值，当消费者每一个轮次消费对应阀值的block后 fetcher 进入下一轮次
)

//Fetcher fabric block Fetcher
type Fetcher struct {
	FabricSdk         *fabsdk.FabricSDK
	Client            *ledger.Client
	SendBlockInfo     chan *provider.SendInfo
	LatestBlockHeight uint64
	QueryHeight       uint64 //当前的迁移高度
	CurrentHeight     uint64 //当前的迁移高度
	StartHeight       uint64 //本阶段的起始高度
	PreBlockHash      []byte //前一个拉取的BlockHash
	logger            *zap.SugaredLogger
	TransferHeight    uint64
	Pool              *ants.Pool
	signal            chan struct{}
	peers             []fab.Peer
	nextRound         chan struct{}
	ThresholdValue    uint64
}

type SendInfo struct {
	BInfo   *transfer.BlockInfo //这里就是强耦合的
	Message string
	Done    bool
}

func NewFetcher(height uint64, pre []byte, chainId, userName, path string, send chan *provider.SendInfo,
	logger *zap.SugaredLogger) (*Fetcher, error) {
	var sdk *fabsdk.FabricSDK
	var client *ledger.Client
	var err error
	var TransferHeight uint64
	//init vars
	if configL.TransferConfig != nil {
		RoundNum = configL.TransferConfig.Async.RoundNum
		ThresholdRound = configL.TransferConfig.Async.ThresholdRound
		TransferHeight = configL.TransferConfig.OriginChain.TransferHeight
	}

	sdk, err = fabsdk.New(config.FromFile(path))
	if err != nil {
		return nil, err
	}

	cc := sdk.ChannelContext(chainId, fabsdk.WithUser(userName))
	client, err = ledger.New(cc)
	if err != nil {
		return nil, err
	}

	return &Fetcher{
		logger:         logger,
		FabricSdk:      sdk,
		Client:         client,
		SendBlockInfo:  send,
		StartHeight:    height,
		CurrentHeight:  height,
		QueryHeight:    height,
		PreBlockHash:   pre,
		TransferHeight: TransferHeight,
		signal:         make(chan struct{}),
		nextRound:      make(chan struct{}),
		ThresholdValue: uint64(RoundNum) + height,
	}, nil
}

//IsFinish judge h whether finish block height
func (p *Fetcher) IsFinish(h uint64) bool {
	if p.TransferHeight > 0 {
		if h >= p.TransferHeight {
			return true
		} else {
			return false
		}
	} else if atomic.CompareAndSwapUint64(&h, p.LatestBlockHeight-1, h) {
		return true
	}

	return false
}

//UpdateState update state
func (p *Fetcher) UpdateState() {
	p.CurrentHeight++
	p.QueryHeight++
	return
}

func (p *Fetcher) Start(ctx context.Context) error {
	chainInfo, err := p.Client.QueryInfo()
	if err != nil {
		return err
	}

	p.logger.Infof("fabric last block height[%d]", chainInfo.BCI.Height)

	if p.StartHeight+1 >= chainInfo.BCI.Height {
		p.CloseSdk()
		return errors.New("already is latest height")
	}
	if p.TransferHeight >= chainInfo.BCI.Height {
		p.CloseSdk()
		return errors.New("transfer height more than latest height")
	}
	if p.TransferHeight > 0 && p.StartHeight >= p.TransferHeight {
		p.CloseSdk()
		return errors.New("start transfer height more than transfer height")
	}
	p.LatestBlockHeight = chainInfo.BCI.Height
	go p.parseLoop(ctx, configL.TransferConfig.Async.QueryCapable)
	return nil
}

//QueryBlockByHeight query block by height PwRp0YvKe803
func (p *Fetcher) QueryBlockByHeight(height uint64) (*common.Block, error) {
	return p.Client.QueryBlock(height)
}

func (p *Fetcher) GetLastBlockHeight() (uint64, error) {
	return p.LatestBlockHeight, nil
}

//QueryBlockByHeight query block by height PwRp0YvKe803
func (p *Fetcher) QueryBlockByHeightByOptions(height uint64, options ...ledger.RequestOption) (*common.Block, error) {
	return p.Client.QueryBlock(height, options...)
}

//Close close fabric and release ants pool
func (p *Fetcher) Close() {
	p.logger.Debugf("close fabric sdk and release goRoutine pool")
	p.FabricSdk.Close()
	p.Pool.Release()
	//_, ok := <-p.signal
	//if ok {
	close(p.signal)
	//}
}

func (p *Fetcher) CloseSdk() {
	p.logger.Debugf("close fabric sdk")
	p.FabricSdk.Close()
}

//NextRound enter next query round
func (p *Fetcher) NextRound() {
	p.logger.Debugf("receive next round signal")
	p.nextRound <- struct{}{}
}

/*
* 1: 循环拉取fabric链网络的区块
* 2：
 */
func (p *Fetcher) parseLoop(ctx context.Context, poolCapacity int) {
	p.logger.Debugf("start loop query fabric block module,start height[%d] query height[%d]", p.CurrentHeight, p.QueryHeight)
	var err error
	p.Pool, err = ants.NewPool(poolCapacity, ants.WithPreAlloc(true))
	if err != nil {
		panic("new ants goroutine pool failed,err: " + err.Error())
	}
	signal := make(chan struct{})

	//peers := configL.TransferConfig.OriginChain.Fabric.Peers
	//capable := uint64(len(peers))

	for {
		select {
		case <-ctx.Done():
			p.logger.Infof("close fabric fetch goroutine %s", ctx.Err())
			p.Close()
			return

		case <-signal:
			p.logger.Debugf("recv close signal,wait query block over")
			<-p.signal //wait 全部的query fabric block协程完成，并且下一阶段接受到最后一个block。由消费者管理close
			p.logger.Debugf("recv close signal,close query loop ")
			//p.CloseSdk()

			return

		case <-p.nextRound:
			p.logger.Debugf("enter next query round")
			p.ThresholdValue += uint64(RoundNum)

		default:
			if p.ThresholdValue <= p.CurrentHeight {
				p.logger.Debugf("query fabric block too fast wait 50 ms")
				time.Sleep(50 * time.Millisecond) //sleep and wait consumer
				continue
			}

			p.CurrentHeight++
			if p.CurrentHeight >= p.LatestBlockHeight {
				chainInfo, err := p.Client.QueryInfo()
				if err != nil { //记录查询
					p.logger.Error(fmt.Errorf("fabric client query channelInfo happen fail,query height is [%d],"+
						"err [%s]", p.CurrentHeight, err))
					time.Sleep(time.Second) //可能是网络问题，休眠一段时间重新链接
					continue
				}

				if p.CurrentHeight >= chainInfo.BCI.Height {
					p.logger.Infof("already query latest fabric block height")
					close(signal)
					continue
				}
				atomic.SwapUint64(&p.LatestBlockHeight, chainInfo.BCI.Height)
			}

			if p.TransferHeight > 0 && p.CurrentHeight > p.TransferHeight {
				p.logger.Infof("arrive transfer fabric block height[%d]", p.TransferHeight)
				close(signal)
				continue
			}

			p.Pool.Submit(func() { //由于ants内部任务是随机的，所以我们使用原子变量来存储高度
				height := atomic.AddUint64(&p.QueryHeight, 1)
				for {
					startT := time.Now()

					//index := height % capable
					block, err := p.Client.QueryBlock(height)
					if err != nil { //记录查询
						p.logger.Error(fmt.Errorf("fabric client query block happen fail,query height is [%d],"+
							"err [%s]", height, err))

						time.Sleep(100 * time.Millisecond) //可能是网络问题，休眠一段时间并重新查询
						continue
					}
					p.logger.Debugf("query block height[%d] end spend time[%+v] ", height, time.Since(startT))

					//bInfo := &SendInfo{
					//	BInfo: &transfer.BlockInfo{
					//		Block: block,
					//	},
					//}

					p.SendBlockInfo <- &provider.SendInfo{
						Height: block.Header.Number,
						Block:  block,
					}
					return
				}

				//info := fmt.Sprintf("The maximum number of retries exceeded,query block[%d]", height)
				//p.logger.Infof(info)
				//close(signal)
				//bInfo := &SendInfo{
				//	BInfo: nil,
				//	Message: info,
				//}
				//p.SendBlockInfo <- bInfo
			})

		}
	}

}
