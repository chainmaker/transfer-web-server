package fabric

import (
	"encoding/base64"
	"strings"

	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/pkg/errors"
)

// Blocklator for block translate
type Blocklator struct {
	block *common.Block
}

// NewBlocklator return new Blocklator
func NewBlocklator(raw []byte) (*Blocklator, error) {
	cb := &common.Block{}
	err := proto.Unmarshal(raw, cb)
	if err != nil {
		return nil, err
	}
	return &Blocklator{
		block: cb,
	}, nil
}

// NewBlocklator return new Blocklator
func NewBlocklatorWithBlock(cb *common.Block) *Blocklator {
	return &Blocklator{
		block: cb,
	}
}

// GetBlockNum return block num
func (bl *Blocklator) GetBlockNum() uint64 {
	return bl.block.Header.Number
}

// GetBlockPrehash return block previoous hash
func (bl *Blocklator) GetBlockPrehash() string {
	hash := bl.block.Header.PreviousHash
	// return strings.ToUpper(hex.EncodeToString(hash))
	return strings.ToUpper(base64.StdEncoding.EncodeToString(hash))
}

// GetChannel return channel id
func (bl *Blocklator) GetChannel() (string, error) {
	if len(bl.block.Data.Data) < 1 {
		return "", errors.New("invalid block")
	}
	env := &common.Envelope{}
	err := proto.Unmarshal(bl.block.Data.Data[0], env)
	if err != nil {
		return "", err
	}
	payload := &common.Payload{}
	err = proto.Unmarshal(env.Payload, payload)
	if err != nil {
		return "", err
	}
	ch := &common.ChannelHeader{}
	err = proto.Unmarshal(payload.Header.ChannelHeader, ch)
	if err != nil {
		return "", err
	}
	return ch.ChannelId, nil
}

// GetMetaDataLastConfig get the last config block num
func (bl *Blocklator) GetMetaDataLastConfig() (uint64, error) {
	if len(bl.block.Metadata.Metadata) > int(common.BlockMetadataIndex_LAST_CONFIG) {
		meta := bl.block.Metadata.Metadata[common.BlockMetadataIndex_LAST_CONFIG]
		cmeta := &common.Metadata{}
		err := proto.Unmarshal(meta, cmeta)
		if err != nil {
			return 0, errors.Wrap(err, "unmarshal metadata error")
		}
		index := cmeta.Value
		cindex := &common.LastConfig{}
		err = proto.Unmarshal(index, cindex)
		if err != nil {
			return 0, errors.Wrap(err, "unmarshal lastconfig error")
		}
		return cindex.Index, nil
	}
	return 0, errors.New("no metadata for lastconfig")
}

// GetMetaDataTransFilter 获取交易有效性标志
func (bl *Blocklator) GetMetaDataTransFilter() ([]bool, error) {
	txfilters := []bool{}
	if len(bl.block.Metadata.Metadata) > int(common.BlockMetadataIndex_TRANSACTIONS_FILTER) {
		filter := bl.block.Metadata.Metadata[common.BlockMetadataIndex_TRANSACTIONS_FILTER]
		for _, f := range filter {
			tf := false
			if peer.TxValidationCode(f) == peer.TxValidationCode_VALID {
				tf = true
			}
			txfilters = append(txfilters, tf)
		}
		return txfilters, nil
	}
	return nil, errors.New("no metadata for transaction filter")
}

// GetMetaDataTransValidationCode 获取交易有效性标志
func (bl *Blocklator) GetMetaDataTransValidationCode() ([]peer.TxValidationCode, error) {
	txfilters := []peer.TxValidationCode{}
	if len(bl.block.Metadata.Metadata) > int(common.BlockMetadataIndex_TRANSACTIONS_FILTER) {
		filter := bl.block.Metadata.Metadata[common.BlockMetadataIndex_TRANSACTIONS_FILTER]
		for _, f := range filter {
			txfilters = append(txfilters, peer.TxValidationCode(f))
		}
		return txfilters, nil
	}
	return nil, errors.New("no metadata for transaction filter")
}

// GetCommitHash get commit hash of the block
func (bl *Blocklator) GetCommitHash() (string, error) {
	if len(bl.block.Metadata.Metadata) > int(common.BlockMetadataIndex_COMMIT_HASH) {
		meta := &common.Metadata{}
		hash := bl.block.Metadata.Metadata[common.BlockMetadataIndex_COMMIT_HASH]
		err := proto.Unmarshal(hash, meta)
		if err != nil {
			return "", err
		}
		// return strings.ToUpper(hex.EncodeToString(meta.Value)), nil
		return strings.ToUpper(base64.StdEncoding.EncodeToString(meta.Value)), nil
	}
	return "", errors.New("no commit hash,invalid block metadata")
}

// GetConfig get config from block  TODO add check block whether is configBlock
func (bl *Blocklator) GetConfig() (*common.Config, *Envelope, error) {
	if len(bl.block.Data.Data) > 1 || len(bl.block.Data.Data) < 1 {
		return nil, nil, nil
	}

	env := &common.Envelope{}
	err := proto.Unmarshal(bl.block.Data.Data[0], env)
	if err != nil {
		return nil, nil, err
	}

	innerEnv := &Envelope{
		Signature: env.Signature,
	}
	payload := &common.Payload{}
	err = proto.Unmarshal(env.Payload, payload)
	if err != nil {
		return nil, nil, err
	}

	ch := &common.ChannelHeader{}
	err = proto.Unmarshal(payload.Header.ChannelHeader, ch)
	if err != nil {
		return nil, nil, err
	}

	innerEnv.Payload.Header.ChannelHeader = ch

	if ch.Type == int32(common.HeaderType_CONFIG) || ch.Type == int32(common.HeaderType_CONFIG_UPDATE) {
		//sh := &common.SignatureHeader{}
		//err = proto.Unmarshal(payload.Header.SignatureHeader, sh)
		//if err != nil {
		//	return nil, nil, err
		//}
		//innerEnv.Payload.Header.SignatureHeader = sh

		cfgenv := &common.ConfigEnvelope{}
		err = proto.Unmarshal(payload.Data, cfgenv)
		if err != nil {
			return nil, nil, err
		}

		return cfgenv.Config, innerEnv, nil

	} else {
		return nil, nil, nil
	}

}

//关于lscc 交易就是证书修改的合约,合约创建等交易只是简单的背书交易，不是config交易
//ParseBlock parse fabric block
func ParseBlock(block *common.Block) (*BlockInfo, error) {
	bInfo := &BlockInfo{
		Block: block,
		Envs:  make([]*Envelope, len(block.Data.Data)),
	}

	bl := NewBlocklatorWithBlock(block)
	codes, err := bl.GetMetaDataTransValidationCode()
	if err != nil {
		return nil, err
	}
	bInfo.TxValidationCode = codes

	conf, cEnv, err := bl.GetConfig()
	if err != nil {
		return nil, err
	}
	if conf != nil {
		bInfo.IsConfig = true
		bInfo.Envs[0] = cEnv
		return bInfo, nil
	}

	for k, d := range block.Data.Data {
		env := &common.Envelope{}
		err := proto.Unmarshal(d, env)
		if err != nil {
			return nil, err
		}
		ts, err := NewTranslator(env)
		if err != nil {
			return nil, err
		}
		bInfo.Envs[k] = ts.innerEnv
	}
	return bInfo, nil
}
