/**
 * @Author: starxxliu
 * @Date: 2022/1/7 7:14 下午
 */

package checker

import "testing"

/*
	createTxBatch 覆盖区块类型。
	   1）genesis block
	   2) config block
	   3）invalid block (尽量覆盖全部无效交易流程)
	   4）正常block
	   5）instant chaincode block
	   6) upgrade chaincode block
	预期结果：
       1）genesis block 中genesis tx 做为交易中的一个字段的value  ————> 不产生任何的读写集
	   2) config block。普通的迁移辅助合约调用交易。parameter 标注是config_tx ————> 不产生任何的读写集
       3）invalid block (尽量覆盖全部无效交易流程) 普通的迁移辅助合约调用交易,parameter 标注是invalid tx,交易重复交易，增加新字段，并且————> 不产生任何的读写集
	   4）正常block  正常的合约调用tx
	   5）instant chaincode block chainmaker install contract tx
	   6) upgrade chaincode block 正常的合约调用tx
测试覆盖面
1： 上述正常交易
2： 模拟部分错误交易，检测预期外情况被过滤。
*/
func TestCheckBlock(t *testing.T) {

}
