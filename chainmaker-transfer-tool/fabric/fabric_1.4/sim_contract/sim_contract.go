package sim_contract

import (
	"encoding/json"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/transfer-tool/common"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	"github.com/hyperledger/fabric-protos-go/ledger/rwset/kvrwset"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

var (
	paramWSetName = "tx_wset"
	OriginTx      = "origin_tx"
	IsValid       = "is_valid"
	InValid       = "invalid"
	TxType        = "tx_type"
	ConfigTx      = "config"
)

func CreateWriteKeyMap(txWriteKeyMap map[string]*commonPb.TxWrite, wSet map[string][]byte) error {
	res := wSet[paramWSetName]
	if res == nil {
		return nil
	}

	fWSet := make(map[string][]byte, 0)
	err := json.Unmarshal(res, &fWSet)
	if err != nil {
		return err
	}

	for k, writes := range fWSet {
		kvWSet := make([]*kvrwset.KVWrite, 0)
		err := json.Unmarshal(writes, &kvWSet)
		if err != nil {
			return err
		}
		for _, kv := range kvWSet {
			value := kv.Value
			if kv.IsDelete {
				value = nil
			}

			txWriteKeyMap[common.ConstructKey(k, []byte(kv.Key))] = &commonPb.TxWrite{
				Key:          []byte(kv.Key),
				Value:        value,
				ContractName: k,
			}
		}
	}

	return nil
}

func CreateTxWriteKeyMap(txWriteKeyMap map[string]*commonPb.TxWrite, wSet map[string][]byte, txRwSet *rwsetutil.TxRwSet) error {
	res := wSet[OriginTx]
	if res == nil {
		return nil
	}

	if string(wSet[TxType]) == ConfigTx { //如果是config 交易txRwSet为nil
		return nil
	}

	if string(wSet[IsValid]) == InValid {
		return nil
	}

	var err error
	if txRwSet == nil {
		txRwSet, err = utils.ParseFabricTx(res)
		if err != nil {
			return err
		}
	}

	fWSet, err := utils.GetWSet(txRwSet)
	if err != nil {
		return err
	}

	for k, writes := range fWSet {
		//kvWSet := make([]*kvrwset.KVWrite, 0)
		//err := json.Unmarshal(writes, &kvWSet)
		//if err != nil {
		//	return err
		//}
		for _, kv := range writes {
			value := kv.Value
			if kv.IsDelete {
				value = nil
			}

			txWriteKeyMap[common.ConstructKey(k, []byte(kv.Key))] = &commonPb.TxWrite{
				Key:          []byte(kv.Key),
				Value:        value,
				ContractName: k,
			}
		}
	}

	return nil
}
