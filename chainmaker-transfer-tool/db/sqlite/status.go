/**
 * @Author: starxxliu
 * @Date: 2021/11/21 1:45 下午
 */

package sqlite

import "gorm.io/gorm"

type Status struct {
	Id              int64  `gorm:"column:id"`
	SaveHeight      uint64 `gorm:"column:save_height"`
	PreHash         string `gorm:"column:pre_hash"`     //fabric pre hash 当前暂时未使用
	NewPreHash      string `gorm:"column:new_pre_hash"` //chainMaker pre hash
	EnableShortCert bool   `gorm:"column:enable_short_cert"`
	CertHash        string `gorm:"column:cert_hash"`
}

func init() {
	con := &Status{}

	RegisterTable(con)
}

func (u *Status) TableName() string {
	return "status"
}

func (u *Status) InsertStatus(conn *gorm.DB) (*Status, error) {
	err := conn.Table(u.TableName()).Create(u).Error
	return u, err
}

func GetStatusAndPaging(offset, limit int, conn *gorm.DB) ([]*Status, error) {
	var certs []*Status
	err := conn.Table("status").Limit(limit).Offset(offset).Find(&certs).Error

	return certs, err
}

func GetStatus(conn *gorm.DB, id string) (*Status, error) {
	var status Status
	err := conn.Where("id = ?", id).First(&status).Error

	return &status, err
}

func (u *Status) UpdateChain(conn *gorm.DB) error {
	err := conn.Model(u).Updates(u).Error
	return err
}

func DropStatus(conn *gorm.DB) error {
	return conn.Exec("DROP TABLE status").Error
}
