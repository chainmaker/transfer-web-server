/**
 * @Author: starxxliu
 * @Date: 2021/12/15 4:22 下午
 */

package utils

import (
	"errors"
	"fmt"

	"github.com/hyperledger/fabric-protos-go/ledger/rwset/kvrwset"

	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

const (
	SysLscc = "lscc"
)

func ParseFabricTx(txByte []byte) (*rwsetutil.TxRwSet, error) {
	env := &common.Envelope{}
	err := proto.Unmarshal(txByte, env)
	if err != nil {
		return nil, err
	}

	payload := &common.Payload{}
	err = proto.Unmarshal(env.Payload, payload)
	if err != nil {
		return nil, err
	}

	trans := &peer.Transaction{}
	err = proto.Unmarshal(payload.Data, trans)
	if err != nil {
		return nil, err
	}

	if len(trans.Actions) < 1 {
		return nil, errors.New("no transaction in block")
	}

	ccp := &peer.ChaincodeActionPayload{}
	err = proto.Unmarshal(trans.Actions[0].Payload, ccp)
	if err != nil {
		return nil, err
	}

	ccresp := &peer.ProposalResponsePayload{}
	err = proto.Unmarshal(ccp.Action.ProposalResponsePayload, ccresp)
	if err != nil {
		return nil, err
	}

	cca := &peer.ChaincodeAction{}
	err = proto.Unmarshal(ccresp.Extension, cca)
	if err != nil {
		return nil, err
	}

	txRWSet := &rwsetutil.TxRwSet{}
	if err = txRWSet.FromProtoBytes(cca.Results); err != nil {
		return nil, fmt.Errorf("%s , txRWSet.FromProtoBytes failed", err)
	}

	return txRWSet, nil
}

func GetWSet(txRwSet *rwsetutil.TxRwSet) (map[string][]*kvrwset.KVWrite, error) {
	wSet := make(map[string][]*kvrwset.KVWrite, 0)
	var err error
	for _, v := range txRwSet.NsRwSets {
		if filterSysContractName(v.NameSpace) {
			continue
		}
		for _, wSet := range v.KvRwSet.Writes {
			wSet.Key, err = FixKey(wSet.Key)
			if err != nil {
				return nil, err
			}
		}

		//wsetByte, err := json.Marshal(v.KvRwSet.Writes)
		//if err != nil {
		//	return nil, err
		//}

		wSet[v.NameSpace] = v.KvRwSet.Writes
	}
	return wSet, nil
}

func filterSysContractName(name string) bool {
	if name == SysLscc {
		return true
	}
	return false
}
