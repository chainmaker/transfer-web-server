/**
 * @Author: starxxliu
 * @Date: 2021/12/9 2:24 下午
 */

package utils

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSplitPath(t *testing.T) {
	path, file := SplitPath("/Users/gopath/src/github.com")
	require.Equal(t, "/Users/gopath/src", path)
	require.Equal(t, "github.com", file)
}
