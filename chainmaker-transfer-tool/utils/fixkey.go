/**
 * @Author: starxxliu
 * @Date: 2021/11/30 3:15 下午
 */

package utils

import (
	"strings"
	"unicode/utf8"
)

const (
	minUnicodeRuneValue   = 0            //U+0000
	maxUnicodeRuneValue   = utf8.MaxRune //U+10FFFF - maximum (and unallocated) code point
	compositeKeyNamespace = "\x00"
	chainMakerSeparate    = "#"
	newContractSeparate   = "_"
)

//fixKey 修改fabric的联合主键，变为chainmaker 可以使用的
//由于chainmaker 只支持一级field,而fabric 却没有这个限制，所以我们将fabric的所有联合主键合并为一个field,中间使用
//'#'作为分隔符
func FixKey(key string) (string, error) {
	if !strings.HasPrefix(key, compositeKeyNamespace) { //如果不是包含
		return key, nil
	}
	key, attributes, err := splitCompositeKey(key)
	if err != nil {
		return "", err
	}
	if len(attributes) == 0 {
		return key, nil
	}
	key = strings.TrimPrefix(key, compositeKeyNamespace) + chainMakerSeparate
	for _, v := range attributes {
		key += v + newContractSeparate
	}

	key = strings.TrimSuffix(key, newContractSeparate)

	return key, nil
}

func splitCompositeKey(compositeKey string) (string, []string, error) {
	componentIndex := 1
	components := []string{}
	for i := 1; i < len(compositeKey); i++ {
		if compositeKey[i] == minUnicodeRuneValue {
			components = append(components, compositeKey[componentIndex:i])
			componentIndex = i + 1
		}
	}
	if len(components) == 0 {
		return compositeKey, components, nil
	}

	return components[0], components[1:], nil
}
