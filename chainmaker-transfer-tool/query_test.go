///**
// * @Author: starxxliu
// * @Date: 2021/12/7 9:06 下午
// */
//
package main

//
//import (
//	"bytes"
//	"chainmaker.org/chainmaker/pb-go/v2/common"
//	"context"
//	"encoding/hex"
//	"fmt"
//	"github.com/gogo/protobuf/proto"
//	commonf "github.com/hyperledger/fabric-protos-go/common"
//	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
//	"github.com/stretchr/testify/require"
//	commonL "chainmaker.org/chainmaker/transfer-tool/common"
//	"chainmaker.org/chainmaker/transfer-tool/common/transfer"
//	"chainmaker.org/chainmaker/transfer-tool/config"
//	"chainmaker.org/chainmaker/transfer-tool/db/sqlite"
//	"chainmaker.org/chainmaker/transfer-tool/loggers"
//	"chainmaker.org/chainmaker/transfer-tool/parse_fabric/chainMaker"
//	"chainmaker.org/chainmaker/transfer-tool/parse_fabric/fabric"
//	"chainmaker.org/chainmaker/transfer-tool/save"
//	"chainmaker.org/chainmaker/transfer-tool/simulation"
//	"go.uber.org/zap"
//	"io/ioutil"
//	"os"
//	"strconv"
//	"testing"
//)
//
//
//func TestQueryBlock(t *testing.T) {
//	errorC := make(chan error, 1)
//	config.InitConfig("./config_path", config.GetConfigEnv())
//
//	plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_SAVE))
//	store, err := save.NewStoreImpl(nil, plog, errorC,nil)
//	require.Nil(t, err)
//
//	height := 3660926
//	block := store.GetBlockWithRWSetsByHeight(uint64(height))
//	println(block)
//
//	blockByte,err := proto.Marshal(block)
//	require.Nil(t, err)
//
//	fileName := "./config_path/testdata/chainMaker_"+strconv.Itoa(int(height))+".proto"
//	err = ioutil.WriteFile(fileName,blockByte,777)
//	require.Nil(t, err)
//}
//
//func TestQueryKey(t *testing.T) {
//	errorC := make(chan error, 1)
//	config.InitConfig("./config_path", config.GetConfigEnv())
//
//	plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_SAVE))
//	store, err := save.NewStoreImpl(nil, plog, errorC,nil)
//	require.Nil(t, err)
//	cs := store.GetStore()
//	contract := ""
//	key := ""
//
//	value,err := cs.ReadObject(contract,[]byte(key))
//	require.Nil(t, err)
//
//	fmt.Println("%+v", string(value))
//}
//
//func TestQueryFabricBlock(t *testing.T) {
//	height := uint64(3079158)
//	config.InitConfig("./config_path", config.GetConfigEnv())
//
//	fetcher, err := fabric.NewFetcher(0, nil, config.TransferConfig.Fabric.ChainName,
//		config.TransferConfig.Fabric.UserName, config.TransferConfig.Fabric.ConfigPath, nil, nil)
//	require.Nil(t, err)
//
//	peers := config.TransferConfig.Fabric.Peers
//
//	block, err := fetcher.QueryBlockByHeightByOptions(height,ledger.WithTargetEndpoints(peers[1])) //query fabric block for height
//	require.Nil(t, err)
//
//	blockByte, err := proto.Marshal(block)
//	require.Nil(t, err)
//
//	fileName := "./config_path/testdata/fabric_"+strconv.Itoa(int(height))+".proto"
//	err = ioutil.WriteFile(fileName,blockByte,777)
//	require.Nil(t, err)
//}
//
//
//func TestCreateFirstTxBatch(t *testing.T) {
//	config.InitConfig("./config_path", config.GetConfigEnv())
//
//	loggers.SetLogConfig(&config.TransferConfig.LogConfig)
//
//	logger := loggers.GetLogger(loggers.MODULE_MAIN)
//
//	sqlite.StartSqliteConnect("")
//	defer func() {
//		f ,_ := os.Open("./sqlite.db")
//
//		if f != nil{
//			os.Remove("./sqlite.db")
//		}
//	}()
//	setHashAlgo()
//	txBatchImpl, err := chainMaker.NewTxBatchImpl(config.TransferConfig, logger, nil)
//	require.Nil(t, err)
//
//	bs := make([]*commonf.Block, 0, 2)
//	for i := 0; i < 2; i++ {
//		height := i
//		fileName := "./config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
//		blockByte0, err := ioutil.ReadFile(fileName)
//		require.Nil(t, err)
//
//		block0 := &commonf.Block{}
//		err = proto.Unmarshal(blockByte0, block0)
//
//		require.Nil(t, err)
//		bs = append(bs, block0)
//	}
//
//	txs, err := txBatchImpl.CreateFirstTxBatch(bs)
//	require.Nil(t, err)
//
//	require.Equal(t,2,len(txs))
//	require.Equal(t,5,len(txs[0].Payload.Parameters))
//	require.Equal(t,8,len(txs[1].Payload.Parameters))
//	//txs[0].Payload.Parameters
//	////检查第一笔交易就是安装辅助合约交易
//	require.Equal(t,common.TxType_INVOKE_CONTRACT,txs[0].Payload.TxType)
//	require.Equal(t,"CONTRACT_MANAGE",txs[0].Payload.ContractName)
//	require.Equal(t,"INIT_CONTRACT",txs[0].Payload.Method)
//
//	for _,pair := range txs[0].Payload.Parameters{
//		if pair.Key == "genesis_tx"{
//			require.Equal(t, true,bytes.Equal(pair.Value,bs[0].Data.Data[0]))
//			continue
//		}else if  pair.Key == "CONTRACT_NAME"{
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("assist")))
//			continue
//		}else if pair.Key == "CONTRACT_VERSION"{
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("v1.0.0")))
//			continue
//		}else if pair.Key == "CONTRACT_RUNTIME_TYPE"{
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("DOCKER_GO")))
//			continue
//		}else if pair.Key == "CONTRACT_BYTECODE" {
//			continue
//		}else {
//			t.Errorf("unknow Parameters pair key")
//		}
//	}
//
//	require.Equal(t,common.TxType_INVOKE_CONTRACT,txs[1].Payload.TxType)
//	require.Equal(t,"CONTRACT_MANAGE",txs[1].Payload.ContractName)
//	require.Equal(t,"INIT_CONTRACT",txs[1].Payload.Method)
//
//	for _,pair := range txs[1].Payload.Parameters{
//		if pair.Key == "origin_tx"{
//			require.Equal(t, true,bytes.Equal(pair.Value,bs[1].Data.Data[0]))
//			continue
//		}else if pair.Key == "is_valid" {
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("valid")))
//			continue
//		}else if pair.Key == "tx_type"{
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("normal")))
//			continue
//		}else if pair.Key == "tx_wset" {
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("{}")))
//			continue
//		} else if  pair.Key == "CONTRACT_NAME"{
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("zxcode")))
//			continue
//		}else if pair.Key == "CONTRACT_VERSION"{
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("v1.0.0")))
//			continue
//		}else if pair.Key == "CONTRACT_RUNTIME_TYPE"{
//			require.Equal(t, true,bytes.Equal(pair.Value,[]byte("DOCKER_GO")))
//			continue
//		}else if pair.Key == "CONTRACT_BYTECODE" {
//			continue
//		}else {
//			t.Errorf("unknow Parameters pair key")
//		}
//	}
//}
//
///*
//	覆盖全部流程，并检测最终结果是否一致
// */
//func TestTransferFabricBlock(t *testing.T) {
//	config.InitConfig("./config_path", config.GetConfigEnv())
//
//	loggers.SetLogConfig(&config.TransferConfig.LogConfig)
//
//	logger := loggers.GetLogger(loggers.MODULE_MAIN)
//
//	sqlite.StartSqliteConnect("")
//	defer func() {
//		f ,_ := os.Open("./sqlite.db")
//
//		if f != nil{
//			os.Remove("./sqlite.db")
//		}
//	}()
//	setHashAlgo()
//	//fabric_blocks := []uint64{0,1,2,197672,304079,1157984,1158003}
//	fabric_blocks := []uint64{3469525}
//
//	fBlocks ,err := readFabricBlock(fabric_blocks)
//	require.Nil(t, err)
//
//
//	println(hex.EncodeToString(fBlocks[0].Data.Data[0]))
//	tblocks,err := parseFabricBlock(fBlocks)
//	require.Nil(t, err)
//	fmt.Printf("%+v",tblocks[0].FBlock.TxValidationCode)
//	tblocks,err = createTxBatch(tblocks,logger)
//	require.Nil(t, err)
//
//	tblocks,err = proposing(tblocks)
//	require.Nil(t, err)
//	fmt.Println(hex.EncodeToString(tblocks[0].Proposal.Block.Header.PreBlockHash))
//	//tblocks[0].Proposal.TxsRwSet["28d1632d683c91e721575d2bfb590f61f0cd18e99549aab56babbc43e41b8fcc"].TxWrites[1].Key = []byte("nft_user_key")
//	//rwSetJ, _ := json.Marshal(tblocks[0].Proposal.TxsRwSet["9c6dbcbe72ec9f8eadff10c596a56a263ebb13b00028074a80431c13b4daeb1c"])
//	//println(string(rwSetJ))
//	//err = checkBlock(tblocks)
//	require.Nil(t, err)
//}
//
//
//
//
//
//func readFabricBlock(heights []uint64)([]*commonf.Block,error){
//	blocks := make([]*commonf.Block,0,len(heights))
//	for _,height := range heights{
//		fileName := "./config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
//		blockByte, err := ioutil.ReadFile(fileName)
//		if err != nil{
//			return nil,err
//		}
//
//		block := &commonf.Block{}
//		err = proto.Unmarshal(blockByte, block)
//		if err != nil{
//			return nil,err
//		}
//
//		blocks = append(blocks, block)
//	}
//
//	return blocks,nil
//}
//
//func parseFabricBlock(blocks []*commonf.Block)([]*transfer.TempBlock,error){
//	tempBlocks := make([]*transfer.TempBlock,0,len(blocks))
//	for _,block := range blocks{
//		binfo ,err := fabric.ParseBlock(block)
//		if err != nil{
//			return nil, err
//		}
//		tb := &transfer.TempBlock{
//			FBlock: binfo,
//		}
//		tempBlocks = append(tempBlocks,tb)
//	}
//	return tempBlocks,nil
//}
//
//func createTxBatch(blocks []*transfer.TempBlock,logger *zap.SugaredLogger) ([]*transfer.TempBlock,error) {
//	txBatch,err := chainMaker.NewTxBatchImpl(config.TransferConfig, logger, nil)
//	if err != nil{
//		return nil, err
//	}
//	for _,block := range blocks{
//		txs,err := txBatch.CreateTxBatch(block.FBlock)
//		if err != nil{
//			return nil, err
//		}
//		block.TxBatch = txs
//	}
//	return blocks,nil
//}
//
//
//func proposing(blocks []*transfer.TempBlock) ([]*transfer.TempBlock,error) {
//	core,err := simulation.NewCoreEngine(0,nil,nil,nil,nil)
//	if err != nil{
//		return nil, err
//	}
//
//	for _,block := range blocks{
//		height := block.FBlock.Block.Header.Number
//		_,proposalBlock,err := core.Proposer.Proposing(height,nil,block)
//		if err != nil{
//			return nil, err
//		}
//
//		pblock,err := core.Proposer.HandleProposedBlock(proposalBlock)
//		if err != nil{
//			return nil, err
//		}
//		proposalBlock.Block = pblock
//		block.Proposal = proposalBlock
//
//	}
//	return blocks, nil
//}
//
//
//func checkBlock(blocks []*transfer.TempBlock) error{
//	commonL.ContractList["zxcode"] = 1
//	commonL.ContractList["dcimanager"] = 11157984
//
//	for _,block := range blocks{
//	_ ,err := commonL.CheckBlock(block)
//		if err != nil{
//			return err
//		}
//	}
//	return nil
//}
//
//
//func TestSlice(t *testing.T){
//	test1 :=  make([]int,2,3)
//	test2 := make([]int,4)
//	fmt.Printf("%p\n",&test1)
//	fmt.Printf("%p\n",test1)
//	fmt.Printf("%p\n",&test2)
//	test1 = append(test1,test2...)
//	fmt.Printf("%p\n",&test1)
//	context.Background()
//}
