/**
 * @Author: starxxliu
 * @Date: 2021/11/23 11:27 上午
 */

package common

import (
	//"chainmaker.org/chainmaker/chainconf/v2"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/transfer-tool/common/chainconf"
	"chainmaker.org/chainmaker/transfer-tool/utils"
)

func GetGenesisBlock(genesis string) (*commonPb.Block, []*commonPb.TxRWSet, error) {
	chainConfig, err := chainconf.Genesis(genesis, "")
	if err != nil {
		return nil, nil, err
	}
	return utils.CreateGenesis(chainConfig)
}

func GetGenesisHashAlgo(genesis string) (string, error) {
	chainConfig, err := chainconf.Genesis(genesis, "")
	if err != nil {
		return "", err
	}

	return chainConfig.Crypto.Hash, nil
}
