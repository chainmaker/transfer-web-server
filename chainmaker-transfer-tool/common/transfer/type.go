/**
 * @Author: starxxliu
 * @Date: 2022/1/5 2:06 下午
 */

package transfer

import (
	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

type TempBlock struct {
	Height    uint64
	FBlock    *BlockInfo
	TxBatch   []*commonpb.Transaction
	Proposal  *consensuspb.ProposalBlock
	Names     []string
	Done      bool
	Err       error
	ExtraDate string
	OBlock    interface{} //原链Block
}

//BlockInfo
type BlockInfo struct {
	Block            *common.Block
	Envs             []*Envelope
	TxValidationCode []peer.TxValidationCode
	IsConfig         bool
}

// Envelope clean struct for envelope
type Envelope struct {
	Payload struct {
		Header struct {
			ChannelHeader *common.ChannelHeader
			//SignatureHeader *common.SignatureHeader
		}
		Transaction struct {
			//Header          *common.SignatureHeader
			ChaincodeAction struct {
				Proposal struct {
					Input     *peer.ChaincodeSpec
					InputByte []byte
				}
				Response struct {
					ProposalHash    []byte
					ChaincodeAction *peer.ChaincodeAction
					//ResponseByte    []byte
					TxRwSet *rwsetutil.TxRwSet
				}
				//Endorses []*peer.Endorsement
			}
		}
	}
	Signature []byte
}
