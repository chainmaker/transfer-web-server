/**
 * @Author: starxxliu
 * @Date: 2021/11/17 11:46 上午
 */

package chainMaker

import (
	"errors"
	"fmt"
	"io/ioutil"

	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/transfer-tool/config"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/sdk-go/v2/utils"
)

func (cc *ChainClient) GetEndorsersWithAuthType(hashType crypto.HashType, payload *common.Payload, usernames ...string) ([]*common.EndorsementEntry, error) {
	var endorsers []*common.EndorsementEntry

	for _, admin := range config.TransferConfig.Chain.AdminUsers {
		var entry *common.EndorsementEntry
		var err error
		switch config.TransferConfig.Chain.AuthType {
		case AuthTypeToStringMap[PermissionedWithCert]:

			entry, err = MakeEndorserWithPath(admin.AdminSignKeyFilePath, admin.AdminSignCrtFilePath, payload)
			if err != nil {
				return nil, err
			}

		case AuthTypeToStringMap[PermissionedWithKey]:

			entry, err = MakePkEndorserWithPath(admin.AdminSignKeyFilePath, hashType, admin.OrgId, payload)
			if err != nil {
				return nil, err
			}

		case AuthTypeToStringMap[Public]:
			entry, err = MakePkEndorserWithPath(admin.AdminSignKeyFilePath, hashType, admin.OrgId, payload)
			if err != nil {
				return nil, err
			}

		default:
			return nil, errors.New("invalid authType")
		}
		endorsers = append(endorsers, entry)
	}

	//var entry *common.EndorsementEntry
	//var err error
	//for _, admin := range config.TransferConfig.Chain.AdminUsers {
	//
	//	entry, err = MakePkEndorserWithPath(admin.AdminSignKeyFilePath, hashType, admin.OrgId, payload)
	//	if err != nil {
	//		return nil, err
	//	}
	//
	//	endorsers = append(endorsers, entry)
	//}

	return endorsers, nil
}

func MakeEndorserWithPath(keyFilePath, crtFilePath string, payload *common.Payload) (*common.EndorsementEntry, error) {
	// 读取私钥
	keyPem, err := ioutil.ReadFile(keyFilePath)
	if err != nil {
		return nil, fmt.Errorf("read key file failed, %s", err)
	}

	// 读取证书
	certPem, err := ioutil.ReadFile(crtFilePath)
	if err != nil {
		return nil, fmt.Errorf("read cert file failed, %s", err)
	}

	cert, err := ParseCert(certPem)
	if err != nil {
		return nil, err
	}

	hashAlgo, err := bcx509.GetHashFromSignatureAlgorithm(cert.SignatureAlgorithm)
	if err != nil {
		return nil, err
	}

	var orgId string
	if len(cert.Subject.Organization) != 0 {
		orgId = cert.Subject.Organization[0]
	}

	return MakeEndorser(orgId, hashAlgo, accesscontrol.MemberType_CERT, keyPem, certPem, payload)
}

func MakePkEndorserWithPath(keyFilePath string, hashType crypto.HashType, orgId string,
	payload *common.Payload) (*common.EndorsementEntry, error) {
	keyPem, err := ioutil.ReadFile(keyFilePath)
	if err != nil {
		return nil, fmt.Errorf("read key file failed, %s", err)
	}

	key, err := asym.PrivateKeyFromPEM(keyPem, nil)
	if err != nil {
		return nil, fmt.Errorf("")
	}

	pubKey := key.PublicKey()
	memberInfo, err := pubKey.String()
	if err != nil {
		return nil, err
	}

	return MakeEndorser(orgId, hashType, accesscontrol.MemberType_PUBLIC_KEY, keyPem,
		[]byte(memberInfo), payload)
}

func MakeEndorser(orgId string, hashType crypto.HashType, memberType accesscontrol.MemberType, keyPem,
	memberInfo []byte, payload *common.Payload) (*common.EndorsementEntry, error) {
	var (
		err       error
		key       crypto.PrivateKey
		signature []byte
	)

	key, err = asym.PrivateKeyFromPEM(keyPem, nil)
	if err != nil {
		return nil, err
	}

	signature, err = utils.SignPayloadWithHashType(key, hashType, payload)
	if err != nil {
		return nil, err
	}

	return utils.NewEndorserWithMemberType(orgId, memberInfo, memberType, signature), nil
}

func getAdminKey(filePath string) (crypto.PrivateKey, error) {
	userKeyBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("read user sign key file failed, %s", err.Error())
	}
	return asym.PrivateKeyFromPEM(userKeyBytes, nil)
}

func getAdminCrtByte(filePath string) ([]byte, error) {
	userCrtBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("read user sign crt file failed, %s", err.Error())
	}

	return userCrtBytes, nil

}
