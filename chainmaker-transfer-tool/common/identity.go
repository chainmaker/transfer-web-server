package common

import (
	"fmt"

	"chainmaker.org/chainmaker/protocol/v2"
)

func GetIdentity(authType string, hashType, OrgId, localPrivKeyFile, localPrivKeyPwd, localCertFile string) (identity protocol.SigningMember, err error) {
	switch authType {
	case protocol.PermissionedWithCert, protocol.Identity:
		identity, err = GetSigningMember(hashType, OrgId, localPrivKeyFile, localPrivKeyPwd, localCertFile)
		if err != nil {
			err = fmt.Errorf("initialize identity failed, %s", err.Error())
			return nil, err
		}
	case protocol.PermissionedWithKey, protocol.Public:
		identity, err = GetPkSigningMember(hashType, OrgId, localPrivKeyFile, localPrivKeyPwd, localCertFile)
		if err != nil {
			err = fmt.Errorf("initialize identity failed, %s", err.Error())
			return nil, err
		}
	default:
		err = fmt.Errorf("auth type doesn't exist")
	}
	return
}
