/**
 * @Author: starxxliu
 * @Date: 2021/12/4 5:11 下午
 */

package common

import (
	"bytes"
	"fmt"

	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/transfer-tool/common/transfer"
	"chainmaker.org/chainmaker/transfer-tool/db/sqlite"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

const (
	lsccInstall = "install"
	lsccDeploy  = "deploy"
	lsccUpgrade = "upgrade"
	SysLscc     = "lscc"
	limit       = 10
)

var (
	InstallContractName = syscontract.SystemContract_CONTRACT_MANAGE.String()
	ContractList        = map[string]uint64{}
)

func InitContractList() error {
	conn, err := getContractInfo()
	if err != nil {
		return err
	}
	ContractList = conn
	return nil
}

/*
	fabric block与simulation chainMaker block的一致性校验
	1：检查height
	2: 检查tx number(exception height 1)
    检查交易
	3: 检查每个交易TxId是否一致
	4: 检查每个交易的write 是否一致（特殊情况包括，短证书交易，安装合约交易，无效交易，config交易）
*/
func CheckBlock(info *transfer.TempBlock) ([]string, error) {

	fb := info.FBlock
	pb := info.Proposal

	if pb.Block.Header.BlockHeight == 1 {
		return nil, nil //first transfer block return
	}

	if fb.Block.Header.Number != pb.Block.Header.BlockHeight {
		return nil, fmt.Errorf("fabric block height failed ")
	}

	if pb.Block.Header.BlockHeight != 1 && len(fb.Envs) != len(pb.TxsRwSet) {
		return nil, fmt.Errorf("transfer block and fabric block tx count must equal ")
	}

	return checkTxs(info)
}

func checkTxs(info *transfer.TempBlock) ([]string, error) {

	//syscontract.InitContract_CONTRACT_NAME.String()
	names := []string{}

	wset := info.Proposal.TxsRwSet
	temp := info.FBlock

	if temp.Block.Header.Number == 0 {
		flag := false
		for _, rwset := range wset {
			if len(rwset.TxWrites) == 0 { //必然存在一个write is nil的迁移交易
				flag = true
			}
		}
		if !flag {
			return nil, fmt.Errorf("not expect tx in fabric block [%d]", temp.Block.Header.Number)
		}
		//验证交易是否是短证书交易，与迁移辅助合约交易
		return nil, nil
	}

	for k, env := range temp.Envs {
		// 某些config 交易 txid is nil,example join channel tx
		if env.Payload.Header.ChannelHeader.TxId == "" {
			if len(wset) > 1 { //此时必然是config 交易
				return nil, fmt.Errorf("expect height[%d] is fabric config block", temp.Block.Header.Number)
			}
			for key, rwset := range wset {
				if len(rwset.TxWrites) != 0 {
					return nil, fmt.Errorf("expect height[%d] tx[%s] wset is nil", info.Proposal.Block.Header.BlockHeight, key)
				}
			}
			return nil, nil
		}
		if temp.TxValidationCode[k] == peer.TxValidationCode_DUPLICATE_TXID {
			//如果是多节点同步，这里最好检查对应交易的wset is nil

			continue
		}
		// check invalid fabric tx
		if temp.TxValidationCode[k] != 0 && len(wset[env.Payload.Header.ChannelHeader.TxId].TxWrites) != 0 {
			return nil, fmt.Errorf("expect height[%d] fabric invalid tx[%s] wset is nil", info.Proposal.Block.Header.BlockHeight,
				env.Payload.Header.ChannelHeader.TxId)
		}

		//check config tx
		txType := env.Payload.Header.ChannelHeader.Type
		if (txType == 1 || txType == 2) && len(wset[env.Payload.Header.ChannelHeader.TxId].TxWrites) != 0 {
			return nil, fmt.Errorf("expect height[%d] fabric config tx wset is nil", info.Proposal.Block.Header.BlockHeight)
		}

		//check instantiation and upgrade contract
		cName := env.Payload.Transaction.ChaincodeAction.Proposal.Input.ChaincodeId.Name
		if !filterContractName(cName) {
			return nil, fmt.Errorf("fabric height[%d] a call to an unknown contract occurred [%s] ", info.FBlock.Block.Header.Number, cName)
		}
		args := env.Payload.Transaction.ChaincodeAction.Proposal.Input.Input.Args

		rwSet := wset[env.Payload.Header.ChannelHeader.TxId]

		if cName == SysLscc && (string(args[0]) == lsccDeploy || string(args[0]) == lsccUpgrade) && temp.TxValidationCode[k] == 0 { //如果是与合约实例化，升级相关的交易写集的验证复杂点
			err := checkWset(rwSet, env.Payload.Transaction.ChaincodeAction.Response.TxRwSet, env.Payload.Header.ChannelHeader.TxId, false)
			if err != nil {
				return nil, err
			}
			name := getContractName(info.Proposal.Block.Txs, env.Payload.Header.ChannelHeader.TxId)
			if name != "" {
				ContractList[name] = info.FBlock.Block.Header.Number
				names = append(names, name)
			}
			continue
		}

		if temp.TxValidationCode[k] != 0 {
			return nil, nil
		}
		//check other
		err := checkWset(rwSet, env.Payload.Transaction.ChaincodeAction.Response.TxRwSet, env.Payload.Header.ChannelHeader.TxId, true)
		if err != nil {
			return nil, err
		}
	}
	return names, nil
}

//check normal tx,
func checkWset(rwSet *commonpb.TxRWSet, TxRwSet *rwsetutil.TxRwSet, txId string, normal bool) error {
	txWset := make(map[string]*commonpb.TxWrite, 0)
	for _, wSet := range rwSet.TxWrites {
		if !normal && wSet.ContractName == InstallContractName { //如果是合约信息，写集则不进行匹配检查
			continue
		}
		txWset[wSet.ContractName+string(wSet.Key)] = wSet
	}

	fwset_num := 0
	for _, nsRwSets := range TxRwSet.NsRwSets {
		for _, kvWrite := range nsRwSets.KvRwSet.Writes {
			if !normal && nsRwSets.NameSpace == SysLscc { //如果是lscc则不进行匹配
				continue
			}
			fwset_num++
			Key, _ := utils.FixKey(kvWrite.Key)

			if !bytes.Equal(txWset[nsRwSets.NameSpace+Key].Value, kvWrite.Value) {
				return fmt.Errorf("txid [%s],contract [%s] key [%s] mismatching", txId,
					nsRwSets.NameSpace, Key)
			}
		}
	}
	if fwset_num != len(txWset) {
		return fmt.Errorf("txid[%s] wset mismatching", txId)
	}

	return nil
}

func getContractName(txs []*commonpb.Transaction, txId string) string {
	for _, tx := range txs {
		if tx.Payload.TxId == txId {
			for _, kvs := range tx.Payload.Parameters {
				if kvs.Key == syscontract.InitContract_CONTRACT_NAME.String() {
					return string(kvs.Value)
				}
			}
		}
	}
	return ""
}

func filterContractName(name string) bool {
	if name == SysLscc || ContractList[name] != 0 {
		return true
	}
	return false
}

func getContractInfo() (map[string]uint64, error) {
	db := sqlite.GetDB()

	offset := 0
	conn := make(map[string]uint64, 0)
	for i := 0; ; i++ {
		offset += limit * i
		contracts, err := sqlite.GetContractAndPaging(offset, limit, db)
		if err != nil {
			return nil, err
		}

		for _, v := range contracts {
			conn[v.ContractName] = v.Height
		}
		if len(contracts) < limit {
			return conn, nil
		}
	}
}
