package common

import (
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/protocol/v2"
	"encoding/hex"
	"fmt"
)

var (
	ShortCerts = map[string][]byte{}
)

func NewMemberFromParam(member *accesscontrol.Member, role protocol.Role,
	hashType string)  (protocol.Member, error) {
	switch member.MemberType {
	case accesscontrol.MemberType_CERT,accesscontrol.MemberType_CERT_HASH:

		return newCertMemberFromPb(member,hashType)
	case accesscontrol.MemberType_PUBLIC_KEY:

	    return newPkMemberFromPb(member,role,hashType)
	}

	return nil, nil
}


func newCertMemberFromPb(member *pbac.Member, hashType string) (*certMember, error) {

	if member.MemberType == pbac.MemberType_CERT {
		return newMemberFromCertPem(member.OrgId, hashType, member.MemberInfo, false)
	}

	if member.MemberType == pbac.MemberType_CERT_HASH {
		cert , ok := ShortCerts[hex.EncodeToString(member.MemberInfo)]
		if !ok {
			return nil, fmt.Errorf("query shutCert[%s] is nil",hex.EncodeToString(member.MemberInfo))
		}
		return newMemberFromCertPem(member.OrgId, hashType, cert, true)
	}

	if member.MemberType == pbac.MemberType_ALIAS {
		return newMemberFromCertPem(member.OrgId, hashType, member.MemberInfo, false)
	}

	return nil, fmt.Errorf("setup member failed, unsupport cert member type")
}

func newPkMemberFromPb(member *accesscontrol.Member, role protocol.Role,
	hashType string) (protocol.Member, error) {
	pk, err := asym.PublicKeyFromPEM(member.MemberInfo)
	if err != nil {
		return  nil, err
	}

	pkByte, err := pk.Bytes()
	if err != nil {
		return nil, err
	}

	return newPkMemberFromParam(member.OrgId, pkByte,role,hashType)
}