/**
 * @Author: starxxliu
 * @Date: 2021/12/4 5:11 下午
 */

package checker

import (
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/transfer-tool/provider"
)

const (
	lsccInstall = "install"
	lsccDeploy  = "deploy"
	lsccUpgrade = "upgrade"
	SysLscc     = "lscc"
	limit       = 10
)

var (
	InstallContractName = syscontract.SystemContract_CONTRACT_MANAGE.String()
	ContractList        = map[string]uint64{}
)

func InitContractList(db provider.TransferDB, taskId string) error {
	//conn, err := getContractInfo(db,taskId)
	//if err != nil {
	//	return  err
	//}
	//ContractList = conn
	return nil
}

/*
	fabric block与simulation chainMaker block的一致性校验
	1：检查height
	2: 检查tx number(exception height 1)
    检查交易
	3: 检查每个交易TxId是否一致
	4: 检查每个交易的write 是否一致（特殊情况包括，短证书交易，安装合约交易，无效交易，config交易）
*/
func CheckBlock(info *provider.TempBlock) ([]string, error) {
	return nil, nil
}
