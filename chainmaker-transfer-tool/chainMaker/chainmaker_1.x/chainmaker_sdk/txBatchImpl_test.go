/**
 * @Author: starxxliu
 * @Date: 2022/1/7 5:58 下午
 */

package chainmaker_sdk

import (
	"testing"
)

/*
	createTxBatch 覆盖区块类型。
	   1）genesis block
	   2) config block
	   3）invalid block (尽量覆盖全部无效交易流程)
	   4）正常block
	   5）instant chaincode block
	   6) upgrade chaincode block
	预期：
	   1）genesis block 中genesis tx 做为交易中的一个字段的value
	   2) config block。普通的迁移辅助合约调用交易。parameter 标注是config_tx
       3）invalid block (尽量覆盖全部无效交易流程) 普通的迁移辅助合约调用交易,parameter 标注是invalid tx
	   4）正常block  正常的合约调用tx
	   5）instant chaincode block chainmaker install contract tx
	   6) upgrade chaincode block 正常的合约调用tx
*/
func TestCreateTxBatch(t *testing.T) {

}

/*
   需要覆盖安装合约交易，正常合约调用交易，config交易，无效交易（交易Id重复，交易签名错误）
*/
