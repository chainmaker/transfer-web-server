package chainmaker

import (
	"chainmaker.org/chainmaker-go/pb/protogo/common"
	cmsdk "chainmaker.org/chainmaker-sdk-go"
)

type fetchChainmakerBySdk struct {
	chainClient cmsdk.SDKInterface
}

func newFetchChainmaker(configPath string) (*fetchChainmakerBySdk, error) {

	chainClient, err := cmsdk.NewChainClient(
		cmsdk.WithConfPath(configPath),
	)
	if err != nil {
		return nil, err
	}

	//启用证书压缩（开启证书压缩可以减小交易包大小，提升处理性能）
	//err = chainClient.EnableCertHash()
	//if err != nil {
	//	return nil, err
	//}

	fetchChainmakerBySdk := &fetchChainmakerBySdk{
		chainClient: chainClient,
	}

	return fetchChainmakerBySdk, nil

}

func (fc *fetchChainmakerBySdk) FetchBlockByHeight(height uint64) (*common.BlockInfo, error) {
	block, err := fc.chainClient.GetBlockByHeight(int64(height), true)
	return block, err
}

func (fc *fetchChainmakerBySdk) QueryLatestHeight() (uint64, error) {

	latestBlock, err := fc.chainClient.GetLastBlock(false)
	if err != nil {
		return 0, err
	}

	latestHeight := uint64(latestBlock.Block.Header.BlockHeight)

	return latestHeight, nil
}

func (fc *fetchChainmakerBySdk) Stop() {
	fc.chainClient.Stop()
}
