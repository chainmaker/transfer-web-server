package chainMaker

import (
	chainmaker_1_x "chainmaker.org/chainmaker/transfer-tool/chainMaker/chainmaker_1.x"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"go.uber.org/zap"
)

//包含原链拉取block，解析block
func NewFetcher(version string, opts ...provider.CreateFetcherConfig) (provider.Fetcher, error) {

	config := &provider.FetcherConfig{}
	for _, opt := range opts {
		opt(config)
	}

	switch version {
	default:
		return chainmaker_1_x.NewCreator(config.Height, config.Pre, config.ChainId, config.UserName,
			config.Path, config.Send, config.Logger)
	}
}

//构建目标链的交易batch
func NewTxsCreator(config *config.Config, log *zap.SugaredLogger, version string, certHash []byte, db provider.TransferDB) (provider.ChainMakerTxBatch, error) {
	switch version {
	default:
		return chainmaker_1_x.NewTxsCreator(config, certHash, db, log)
	}
}

func NewChecker(version string, contractList map[string]uint64) (provider.Checker, error) {
	switch version {
	default:
		return chainmaker_1_x.NewChecker(contractList)
	}
}

//NewSimContract 模拟合约执行
func NewSimContract(version string) (provider.SimContractExec, error) {
	switch version {
	default:
		return chainmaker_1_x.NewSimContract()
	}
}
