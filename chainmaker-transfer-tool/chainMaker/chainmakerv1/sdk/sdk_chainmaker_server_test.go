/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_sdk_go

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestChainClient_GetChainMakerServerVersion(t *testing.T) {
	client, err := createClient()
	require.Nil(t, err)
	version, err := client.GetChainMakerServerVersion()
	require.Nil(t, err)
	fmt.Println("get chainmaker server version:", version)
}
