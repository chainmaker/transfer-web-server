module chainmaker.org/chainmaker-go/common

go 1.15

require (
	github.com/btcsuite/btcd v0.21.0-beta
	github.com/davecgh/go-spew v1.1.1
	github.com/fastly/go-utils v0.0.0-20180712184237-d95a45783239 // indirect
	github.com/gogo/protobuf v1.3.1
	github.com/golang/groupcache v0.0.0-20190702054246-869f871628b6
	github.com/golang/protobuf v1.4.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869 // indirect
	github.com/json-iterator/go v1.1.10
	github.com/lestrrat-go/strftime v1.0.3
	github.com/libp2p/go-libp2p-core v0.6.1
	github.com/libp2p/go-openssl v0.0.7
	github.com/miekg/pkcs11 v1.0.3
	github.com/minio/sha256-simd v0.1.1
	github.com/mr-tron/base58 v1.2.0
	github.com/multiformats/go-multiaddr v0.3.1
	github.com/multiformats/go-multihash v0.0.14
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.6.1
	github.com/tebeka/strftime v0.1.5 // indirect
	github.com/tidwall/gjson v1.6.1
	github.com/tidwall/tinylru v1.0.2
	github.com/tjfoc/gmsm v1.4.1
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68
	google.golang.org/grpc v1.34.0
)
