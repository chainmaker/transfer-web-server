/**
 * @Author: starxxliu
 * @Date: 2021/12/29 9:39 下午
 */

package mulnode

import (
	"context"
	"fmt"
	"time"

	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"chainmaker.org/chainmaker/transfer-tool/pb/pb"
	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
)

type Node struct {
	Ip      string
	Port    int
	log     *loggers.CMLogger
	receive chan [][]byte
	ctx     context.Context
	errChan chan error
	client  pb.RpcNodeClient
}

func NewNode(ip string, port int, log *loggers.CMLogger, receive chan [][]byte,
	ctx context.Context, errChan chan error) *Node {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	target := fmt.Sprintf("%s:%d", ip, port)
	conn, err := grpc.Dial(target, opts...)
	if err != nil {
		log.Panicf("grpc dial[%s] fail to dial: %v", target, err)
	}

	client := pb.NewRpcNodeClient(conn)
	node := &Node{
		Ip:      ip,
		Port:    port,
		log:     log,
		receive: receive,
		ctx:     ctx,
		errChan: errChan,
		client:  client,
	}
	return node
}

// Start put block
func (n *Node) Start() {
	n.log.Debugf("start node[%s]", n.Ip)
	go n.loop(n.receive)
	return
}

// PutBlock put block
func (n *Node) PutBlock(pb []byte) error {
	return nil
}

// GetLastBlockHeight get last block
func (n *Node) GetLastBlockHeight() (*commonpb.Block, error) {
	res, err := n.client.GetLastBlockHeight(context.Background(), &pb.BlockHeight{})
	if err != nil {
		return nil, err
	}

	if res.Message != nil {
		block := &commonpb.Block{}
		err = proto.Unmarshal(res.Message, block)
		if err != nil {
			return nil, err
		}
		return block, err
	}

	return nil, nil
}

// GetBlockWithRWSetsByHeight get block
func (n *Node) GetBlockWithRWSetsByHeight(h uint64) (*storePb.BlockWithRWSet, error) {
	res, err := n.client.GetBlockHeightByHeight(context.Background(), &pb.BlockHeight{Height: h})
	if err != nil {
		//n.log.Error("get block from[%s] height[%d] fail,err:%s",n.Ip,h,err.Error())
		return nil, err
	}

	if res.Message != nil {
		info := &storePb.BlockWithRWSet{}
		err = proto.Unmarshal(res.Message, info)
		if err != nil {
			return nil, err
		}
		return info, err
	}

	return nil, nil
}

// InitGenesis initialize genesis block
func (n *Node) InitGenesis(gb []byte) error {
	gbs := [][]byte{gb}
	_, err := n.client.InitGenesis(context.Background(), &pb.BlockRequest{BlockByte: gbs})
	return err
}

// loop receive message
func (n *Node) loop(receive chan [][]byte) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	stream, err := n.client.PutBlock(ctx)
	if err != nil {
		n.log.Panic("create client putBlock stream fail,ip:", n.Ip)
		return
	}

	defer stream.CloseSend()

	for {
		select {
		case <-n.ctx.Done():
			n.log.Infof("node[%s] close send block loop,err[%s]", n.Ip, n.ctx.Err())
			return

		case info := <-receive:
			startT := time.Now()
			if err := stream.Send(&pb.BlockRequest{BlockByte: info}); err != nil { //增加重连机制
				err = fmt.Errorf("node[%s],send block info fail,err[%s]", n.Ip, err.Error())
				n.log.Error(err)
				n.errChan <- err
				return
			}
			n.log.Debugf("node[%s] send info,speed time[%v],current time[%v]", n.Ip, time.Since(startT), time.Now())
		}
	}
}
