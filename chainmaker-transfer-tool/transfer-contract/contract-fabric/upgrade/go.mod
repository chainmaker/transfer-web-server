module github.com/transfer-contract/upgrade

go 1.17

require chainmaker.org/chainmaker-contract-sdk-docker-go v0.0.0-00010101000000-000000000000

require (
	chainmaker.org/chainmaker/common/v2 v2.1.0 // indirect
	chainmaker.org/chainmaker/pb-go/v2 v2.1.0 // indirect
	chainmaker.org/chainmaker/protocol/v2 v2.1.1-0.20211117024857-2641037a7269 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.18.1 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/grpc v1.39.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

replace chainmaker.org/chainmaker-contract-sdk-docker-go => ../../chainmaker-contract-sdk-docker-go



