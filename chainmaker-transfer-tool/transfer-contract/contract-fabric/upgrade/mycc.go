package main

import (
	"chainmaker.org/chainmaker-contract-sdk-docker-go/pb/protogo"
	"chainmaker.org/chainmaker-contract-sdk-docker-go/shim"
	"fmt"
	"log"
	"strconv"
)

type Transfer struct{}

func (t *Transfer) InitContract(stub shim.CMStubInterface) protogo.Response {
	return shim.Success([]byte("Init Success"))
}

func (t *Transfer) InvokeContract(stub shim.CMStubInterface) protogo.Response {

	args := stub.GetArgs()
	method := string(args["method"])

	switch method {

	case "transfer":
		return t.transfer(stub)

	case "query":
		return t.query(stub)

	default:
		msg := "unknow method"
		return shim.Error(msg)

	}

}

func (t *Transfer) transfer(stub shim.CMStubInterface) protogo.Response {

	args := stub.GetArgs()
	accFrom := string(args["acc_from"])
	accTo := string(args["acc_to"])
	amtTrans, err := strconv.Atoi(string(args["amt_trans"]))
	if err != nil {
		return shim.Error(err.Error())
	}

	fromBalStr, err := stub.GetState(accFrom, "")
	if err != nil {
		return shim.Error(err.Error())
	}
	fromBal, err := strconv.Atoi(fromBalStr)
	if err != nil {
		return shim.Error(err.Error())
	}

	toBalStr, err := stub.GetState(accTo, "")
	if err != nil {
		return shim.Error(err.Error())
	}
	toBal, err := strconv.Atoi(toBalStr)
	if err != nil {
		return shim.Error(err.Error())
	}
	if fromBal < amtTrans {
		return shim.Error(fmt.Sprintf("not enough money from_bal:%d, amt_trans:%d", fromBal, amtTrans))
	}

	fromBal -= amtTrans
	toBal += amtTrans

	err = stub.PutState(accFrom, "", strconv.Itoa(fromBal))
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(accTo, "", strconv.Itoa(toBal))
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success([]byte("transfer success"))

}

func (t *Transfer) query(stub shim.CMStubInterface) protogo.Response {

	args := stub.GetArgs()
	acc := string(args["acc"])

	accBal, err := stub.GetState(acc, "")
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte(accBal))

}

func main() {
	err := shim.Start(new(Transfer))
	if err != nil {
		log.Fatal(err)
	}
}
