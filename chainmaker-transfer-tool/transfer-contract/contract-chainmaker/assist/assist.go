/**
 * @Author: starxxliu
 * @Date: 2021/11/29 6:50 下午
 */

package main

import (
	"chainmaker.org/chainmaker-contract-sdk-docker-go/pb/protogo"
	"chainmaker.org/chainmaker-contract-sdk-docker-go/shim"
	"chainmaker.org/chainmaker-go/pb/protogo/common"
	"encoding/json"
	"github.com/golang/protobuf/proto"
	"log"
)

const (
	defaultVersion = "v1.0.0"
)

type AssistContract struct {
}

// KVWrite captures a write (update/delete) operation performed during transaction simulation
//type KVWrite struct {
//	Key                  string   `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
//	IsDelete             bool     `protobuf:"varint,2,opt,name=is_delete,json=isDelete,proto3" json:"is_delete,omitempty"`
//	Value                []byte   `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
//	XXX_NoUnkeyedLiteral struct{} `json:"-"`
//	XXX_unrecognized     []byte   `json:"-"`
//	XXX_sizecache        int32    `json:"-"`
//}

// 部署合约 需要反向验证在fabric install 实力化交易时要怎么做
//实现合约的初始化接口
func (f *AssistContract) InitContract(stub shim.CMStubInterface) protogo.Response {
	return shim.Success([]byte("Init Success"))
}

//实现合约的调用接口
func (f *AssistContract) InvokeContract(stub shim.CMStubInterface) protogo.Response {

	// 获取参数，方法名通过参数传递
	args := stub.GetArgs()
	method := args["method"]

	switch string(method) {
	case "transfer":
		return transfer(stub)
	default:
		return shim.Error("haha" + string(method) + "hainvalid method")
	}
}

//合约的目的：只用来处理读写集的更新
func transfer(stub shim.CMStubInterface) protogo.Response {

	args := stub.GetArgs()
	specialtx := args["special_tx"]

	//1. 如果是specialtx如何处理
	if len(specialtx) > 0 {
		return shim.Success(nil)
	}

	//2. 如果是nornal调用用户合约类型的交易
	//2.1 得到chainmakerv1的读写集
	origin_rwset := args["origin_rwset"]
	//2.2 得到chainmakerv1的写集map，key是合约名，value，写集kv
	wSet, err := getWSetFromRWSet(origin_rwset)
	if err != nil {
		shim.Error("parseFabricTx happen failed,err :" + err.Error())
	}

	//遍历写集map，得到key是合约名，value，写集kv
	for contractName, wsetkv := range wSet {
		//为啥不用proto，因为合约里面没有这种map类型proto的定义
		//对写集的kv进行序列化
		v, _ := json.Marshal(wsetkv)

		wsetkvmap := map[string][]byte{"tx_wset": v}

		//根据合约名和对应的写集kv的map，进行跨合约调用
		res := stub.CallContract(contractName, defaultVersion, wsetkvmap)
		if res.Status != shim.OK {
			shim.Error("assis call contract " + contractName + " happen err:" + res.Message)
		}
	}
	return shim.Success(nil)
}

// 从chainmaker1.x的读写集得到chainmaker的写集，再转换成写集map
func getWSetFromRWSet(rwSetByte []byte) (map[string]map[string][]byte, error) {
	txRWSet := &common.TxRWSet{}
	err := proto.Unmarshal(rwSetByte, txRWSet)
	if err != nil {
		return nil, err
	}

	//将chainmaker1.x的写集放到一个map中，map的key是合约名，map的value是写集的kv，通过wsetmap把写集进行分类、合并
	//因为写集的key不能是不可比较的slice([]byte)，所以把[]byte转换成string
	wsetmap := make(map[string]map[string][]byte)

	for _, wset := range txRWSet.TxWrites {
		wsetmapkv, ok := wsetmap[wset.ContractName]
		//如果wsetmap不存在这个合约就新建一个合约名和并kv赋值
		if !ok {
			wsetmap[wset.ContractName] = map[string][]byte{string(wset.Key): wset.Value}
		} else {
			//如果存在这个合约，就添加这个合约的kv，或者给这个合约已有的key赋新的值
			wsetmapkv[string(wset.Key)] = wset.Value
		}
	}

	return wsetmap, nil
}

func main() {
	err := shim.Start(new(AssistContract))
	if err != nil {
		log.Fatal(err)
	}
}
