package sqlite

import "gorm.io/gorm"

type OriginChainInfo struct {
	Id          int64  `gorm:"column:id"`
	TaskId      string `gorm:"uniqueIndex;column:task_id"`
	ChannelName string `gorm:"column:channel_name"`
	UserName    string `gorm:"column:user_name"`
	SdkPath     string `gorm:"column:sdk_path"`
	CertPath    string `gorm:"column:cert_path"`
}

func init() {
	oci := &OriginChainInfo{}

	RegisterTable(oci)
}

func (oci *OriginChainInfo) TableName() string {
	return "origin_chain_info"
}

func (oci *OriginChainInfo) InsertOriginChainInfo(conn *gorm.DB) (*OriginChainInfo, error) {
	err := conn.Table(oci.TableName()).Create(oci).Error
	return oci, err
}

func (oci *OriginChainInfo) UpdateOriginChainInfo(conn *gorm.DB) error {
	err := conn.Model(oci).Save(oci).Error
	return err
}

func GetOriginChainInfoByTaskId(conn *gorm.DB, taskId string) (*OriginChainInfo, error) {
	var chain OriginChainInfo
	err := conn.Where("task_id = ?", taskId).First(&chain).Error

	return &chain, err
}

func DropOriginChainInfo(conn *gorm.DB) error {
	return conn.Exec("DROP TABLE origin_chain_info").Error
}
