package sqlite

import (
	"github.com/stretchr/testify/require"
	"os"
	"testing"
)

func TestTaskInfo_QueryTasksCount(t *testing.T) {

	StartSqliteConnect("")
	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	ti := TaskInfo{}
	count ,err := ti.QueryTasksCount(GetDB())
	require.Nil(t, err)

	require.Equal(t, 0,count)
}


func TestTaskInfo_QueryTasksCountByStatus(t *testing.T) {

	StartSqliteConnect("")
	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	ti := TaskInfo{}
	count ,err := ti.QueryTasksCountByStatus(GetDB(),5)
	require.Nil(t, err)

	require.Equal(t, 0,count)
}



func TestTaskInfo_QueryTasksCountByChainType(t *testing.T) {
	StartSqliteConnect("")
	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	ti := TaskInfo{}
	tasks ,err := ti.QueryTasksCountByChainType(GetDB(),"fabric")
	require.Nil(t, err)

	require.Equal(t, 0,len(tasks))
}

func TestQueryTasksPagingByNameAndStatus(t *testing.T) {
	StartSqliteConnect("")
	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	ti := &TaskInfo{
		TaskName: "test" ,
		TargetChainVersion: "test",
		OriginChainVersion: "test",
		Status: 5,
	}

	ti,err := ti.InsertTaskInfo(GetDB())
	require.Nil(t, err)

	tis,err := QueryTasksPagingByNameAndStatus(0,10,GetDB(),"",5)
	require.Nil(t, err)
	require.Equal(t, 1,len(tis))

	tis2,err := QueryTasksPagingByNameAndStatus(0,10,GetDB(),"te",5)
	require.Nil(t, err)
	require.Equal(t, 1,len(tis2))


}