/**
 * @Author: starxxliu
 * @Date: 2021/11/30 8:25 下午
 */

package sqlite

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStatus_InsertContract(t *testing.T) {
	//sqlite init
	StartSqliteConnect("./sqlite.db")
	status := &Status{
		TaskId:     "11",
		SaveHeight: 3079157,
		PreHash:    "-----",
	}
	println("----------")
	status, err := status.InsertStatus(GetDB())
	require.Nil(t, err)

	status.SaveHeight = 0
	status.PreHash = ""
	status.CertHash = ""
	//err = status.UpdateChain2(GetDB())
	require.Nil(t, err)

	ss, err := GetStatusByTaskID(GetDB(), "11")
	require.Nil(t, err)

	println(ss)
}

func TestContract(t *testing.T) {
	//sqlite init
	StartSqliteConnect("../../sqlitedb/sqlite.db")

	//err := DropContract(GetDB())
	//require.Nil(t, err)

	contract0 := &Contract{
		ContractName: "zxcode",
		Height:       1,
	}
	c, err := contract0.InsertContract(GetDB())
	require.Nil(t, err)

	contract1 := &Contract{
		ContractName: "dcimanager",
		Height:       1157983,
	}
	println(c.Id)
	c1, err := contract1.InsertContract(GetDB())
	require.Nil(t, err)
	println(c1.Id)
	//contracts ,err := GetContractAndPaging(0,10,GetDB())
	//require.Nil(t, err)
	//
	//println(len(contracts))
}

func TestQueryContract(t *testing.T) {
	//sqlite init
	StartSqliteConnect("../../sqlitedb/sqlite.db")

	contracts, err := GetContractAndPaging(0, 10, "", GetDB())
	require.Nil(t, err)

	println(len(contracts))
}

func TestAddContract(t *testing.T) {
	//sqlite init
	StartSqliteConnect("../../sqlitedb/sqlite.db")
	list := []string{"assist",
		"dcimanager",
		"kvcode",
		"kvcode1",
		"nft",
		"nftdev",
		"nftrayb",
		"nftuser",
		"nftuserv2",
		"testcc",
		"zxcode"}

	for _, key := range list {
		contract := &Contract{
			ContractName: key,
			Height:       3079157,
		}
		c1, err := contract.InsertContract(GetDB())
		require.Nil(t, err)
		println(c1.Id)
	}
}
