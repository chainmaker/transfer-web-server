/**
 * @Author: starxxliu
 * @Date: 2021/8/17 8:40 下午
 */

package redis

import "github.com/gomodule/redigo/redis"

type RedisImpl interface {
	GetPool()*redis.Pool
	Init(url, auth string, db, maxIdle, maxActive, idleTimeout int) error
	Set(key string, data interface{}, time int) error
	SetVal(key, value interface{}, time int) error
	Exists(key string) bool
	Ttl(key string) int
	Get(key string) ([]byte, error)
	GetString(key string) (string, error)
	GetInt(key string) (int, error)
	Delete(key string) (bool, error)
	LikeDeletes(key string) error
	Incr(key string) (int, error)
	GetSumLikeKeys(key string) (int, error)
	Msetns(pars ... interface{}) (int, error)
	Mset(pars ... interface{})  error
	Deletes(key ... interface{}) (int, error)
	GetDBSize()(int, error)
	FlushDB() error
	GetConn() redis.Conn
}
