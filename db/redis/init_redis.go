/**
 * @Author: starxxliu
 * @Date: 2021/8/12 8:51 下午
 */

package redis

import (
	"fmt"

	"chainmaker.org/chainmaker/transfer-web-server/config"
)

var RedisClient *RedisHandler

func RedisInit() {

	RedisClient = NewRedisHandler()
	host := config.KaiwuConfig.DBConfig.Redis.RedisIp
	port := config.KaiwuConfig.DBConfig.Redis.RedisPort

	url := fmt.Sprintf("%s:%d", host, port)

	auth := config.KaiwuConfig.DBConfig.Redis.Auth
	dbName := config.KaiwuConfig.DBConfig.Redis.DB

	// maxidle maxactive 不限制， idleTimeout 300ms
	err := RedisClient.Init(url, auth, dbName, 1500, 1500, 0)
	if err != nil {
		panic(err)
	}
}
