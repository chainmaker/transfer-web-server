/**
 * @Author: starxxliu
 * @Date: 2021/8/13 10:47 上午
 */

package redis

import (
	"chainmaker.org/chainmaker/transfer-web-server/config"
	"github.com/stretchr/testify/require"
	"testing"
)

func startConfig() {
	confPath := "../../config_file"
	env := "dev"
	config.InitConfig(confPath, env)
}

func TestInit(t *testing.T) {
	startConfig()
	RedisInit()
	RedisClient.Msetns("for", "1", "bar", "2")
}

func TestGetDB(t *testing.T) {
	startConfig()
	RedisInit()
	size, err := RedisClient.GetDBSize()
	require.Nil(t, err)
	println(size)
}

func TestGet(t *testing.T) {
	startConfig()
	RedisInit()
	res, err := RedisClient.GetString("nft-1") //55499
	require.Nil(t, err)
	println(res)
}

func BenchmarkRedis(b *testing.B) {
	startConfig()
	RedisInit()
	for i := 0; i < b.N; i++ {
		for j:= 0;j<5;j++{
			go getRedis()
		}

		//_, err1 := RedisClient.Get("sm4-1") //55499,110085
		//if err1 != nil {
		//	println(err)
		//}

		//conn := RedisClient.GetConn()
		//_,err := redis.Int(conn.Do("MSETNX","hashkey","w","txid","w" )) //111483
		//if err != nil{
		//	println(err)
		//}
		//
		//_,err1 := RedisClient.Get("sm4-1")
		//if err1 != nil{
		//	println(err)
		//}
		//conn.Close()
	}
}

func TestREdis(t *testing.T) {
	startConfig()
	RedisGet()
}

func getRedis(){
	_, err := RedisClient.Msetns("hashkey","w","txid","w" ) //55499
	if err != nil {
		println(err)
	}
}

func TestInitRedisData(t *testing.T){
	startConfig()
	RedisInit()
	/*nft1 := `{"attributes":[{"trait_type":"base","value":"starfish"},{"trait_type":"eyes","value":"joy"},{"trait_type":"mouth","value":"surprised"},{"trait_type":"level","value":2},{"trait_type":"stamina","value":2.3},{"trait_type":"personality","value":"sad"},{"display_type":"boost_number","trait_type":"aqua_power","value":40},{"display_type":"boost_percentage","trait_type":"stamina_increase","value":10},{"display_type":"number","trait_type":"generation","value":2}],"description":"神龙，只见其首。龙首相比圆明园原版增加了霸气，象征着神龙元神的龙角也放大到和头部呈2：1的比例，以凸显其威武。龙须霍霍而起，捕获了龙正准备腾飞的那一瞬间，乘风而去。一片片龙鳞雕刻得分明，其中有少数比较突出的逆鳞，是艺术家把最细节都刻画在雕塑中的小心思。这个版本的龙首还包含了龙的半身，是致敬原版圆明园本来十二生肖是个坐像，而这里的龙挣脱了本来的人形的枷锁，重新化为龙形向上腾飞。金龙共发行一枚,详情可去往http://kaiwuart.io/pc/singleDetail?id=1查看","external_url":"http://kaiwuart.io/pc/singleDetail?id=1","image":"http://kaiwuart.io/images/kaiwu/01chenglong_jing.jpg","name":"暴风·辰龙(金)","about":"金龙共发行一枚,详情可去往http://kaiwuart.io/pc/singleDetail?id=1查看"}`
    err := RedisClient.Set("nft-1",nft1,0) //55499*/

	nft2 := `{"attributes":[{"trait_type":"base","value":"starfish"},{"trait_type":"eyes","value":"joy"},{"trait_type":"mouth","value":"surprised"},{"trait_type":"level","value":2},{"trait_type":"stamina","value":2.3},{"trait_type":"personality","value":"sad"},{"display_type":"boost_number","trait_type":"aqua_power","value":40},{"display_type":"boost_percentage","trait_type":"stamina_increase","value":10},{"display_type":"number","trait_type":"generation","value":2}],"description":"神龙，只见其首。龙首相比圆明园原版增加了霸气，象征着神龙元神的龙角也放大到和头部呈2：1的比例，以凸显其威武。龙须霍霍而起，捕获了龙正准备腾飞的那一瞬间，乘风而去。一片片龙鳞雕刻得分明，其中有少数比较突出的逆鳞，是艺术家把最细节都刻画在雕塑中的小心思。这个版本的龙首还包含了龙的半身，是致敬原版圆明园本来十二生肖是个坐像，而这里的龙挣脱了本来的人形的枷锁，重新化为龙形向上腾飞。青铜龙共发行两枚,详情可去往http://kaiwuart.io/pc/singleDetail?id=2查看","external_url":"http://kaiwuart.io/pc/singleDetail?id=1","image":"http://kaiwuart.io/images/kaiwu/02chenglong_qt.jpg","name":"暴风·辰龙(金)","about":"青铜龙共发行两枚,详情可去往http://kaiwuart.io/pc/singleDetail?id=2查看"}`

	err := RedisClient.Set("nft-2",nft2,0) //55499

	require.Nil(t, err)
}