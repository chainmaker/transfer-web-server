/**
 * @Author: starxxliu
 * @Date: 2021/10/8 10:59 上午
 */

package mysql

import (
	"chainmaker.org/chainmaker/transfer-web-server/config"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestAddArtist(t *testing.T)  {
	config.InitConfig("../../config_file", "dev")
	StartMysqlConnect()

	//artist := &NftInfo{
	//	NftKey: 1,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"dragon"},{"trait_type":"material","value":"gold"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.The Kaiwu's version of the dragon is redesigned to be more modern than its original Old Summer Palace version. The horn on the dragon's head was magnified to be in 2:1 ratio compare to the head. The designer captured the moment when the dragon is about to fly when the winds flow through the scale and the hair of the dragon. Only 1 Gold Dragon is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=1","image":"http://kaiwuart.io/images/kaiwu/01chenglong_jing.jpg","name":"暴风·辰龙(金) Dragon in the Storm (Gold)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 2,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"dragon"},{"trait_type":"material","value":"bronze"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.The Kaiwu's version of the dragon is redesigned to be more modern than its original Old Summer Palace version. The horn on the dragon's head was magnified to be in 2:1 ratio compare to the head. The designer captured the moment when the dragon is about to fly when the winds flow through the scale and the hair of the dragon. Only 2 Bronze Dragon is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=2","image":"http://kaiwuart.io/images/kaiwu/02chenglong_qt.jpg","name":"暴风·辰龙(青铜) Dragon in the Storm (Bronze)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 3,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"dragon"},{"trait_type":"material","value":"bronze"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.The Kaiwu's version of the dragon is redesigned to be more modern than its original Old Summer Palace version. The horn on the dragon's head was magnified to be in 2:1 ratio compare to the head. The designer captured the moment when the dragon is about to fly when the winds flow through the scale and the hair of the dragon. Only 2 Bronze Dragon is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=2","image":"http://kaiwuart.io/images/kaiwu/02chenglong_qt.jpg","name":"暴风·辰龙(青铜) Dragon in the Storm (Bronze)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 4,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"dragon"},{"trait_type":"material","value":"copper"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nThe Kaiwu's version of the dragon is redesigned to be more modern than its original Old Summer Palace version. The horn on the dragon's head was magnified to be in 2:1 ratio compare to the head. The designer captured the moment when the dragon is about to fly when the winds flow through the scale and the hair of the dragon.\nOnly 3 Copper Dragon is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=3","image":"http://kaiwuart.io/images/kaiwu/03chenglong_ht.jpg","name":"暴风·辰龙(红铜) Dragon in the Storm (Copper)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 5,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"dragon"},{"trait_type":"material","value":"copper"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nThe Kaiwu's version of the dragon is redesigned to be more modern than its original Old Summer Palace version. The horn on the dragon's head was magnified to be in 2:1 ratio compare to the head. The designer captured the moment when the dragon is about to fly when the winds flow through the scale and the hair of the dragon.\nOnly 3 Copper Dragon is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=3","image":"http://kaiwuart.io/images/kaiwu/03chenglong_ht.jpg","name":"暴风·辰龙(红铜) Dragon in the Storm (Copper)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 6,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"dragon"},{"trait_type":"material","value":"copper"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nThe Kaiwu's version of the dragon is redesigned to be more modern than its original Old Summer Palace version. The horn on the dragon's head was magnified to be in 2:1 ratio compare to the head. The designer captured the moment when the dragon is about to fly when the winds flow through the scale and the hair of the dragon.\nOnly 3 Copper Dragon is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=3","image":"http://kaiwuart.io/images/kaiwu/03chenglong_ht.jpg","name":"暴风·辰龙(红铜) Dragon in the Storm (Copper)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 7,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"snake"},{"trait_type":"material","value":"gold"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, first series of Kaiwu Art NFT.\nKaiwu's version of snake is designed to be a symbol of life. The plants bursting out under the scale of the snake balanced the fierce look of the snake. The snake is designed to be a protector of nature.\nOnly 1 Gold Snake is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=4","image":"http://kaiwuart.io/images/kaiwu/04sishe_jing.jpg","name":"飞砂·巳蛇(金) Snake under the Sands (Gold)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 8,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"snake"},{"trait_type":"material","value":"bronze"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nKaiwu's version of snake is designed to be a symbol of life. The plants bursting out under the scale of the snake balanced the fierce look of the snake. The snake is designed to be a protector of nature.\nOnly 2 Bronze Snake is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=5","image":"http://kaiwuart.io/images/kaiwu/05sishe_qt.jpg","name":"飞砂·巳蛇(青铜) Snake under the Sands (Bronze)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 9,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"snake"},{"trait_type":"material","value":"bronze"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nKaiwu's version of snake is designed to be a symbol of life. The plants bursting out under the scale of the snake balanced the fierce look of the snake. The snake is designed to be a protector of nature.\nOnly 2 Bronze Snake is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=5","image":"http://kaiwuart.io/images/kaiwu/05sishe_qt.jpg","name":"飞砂·巳蛇(青铜) Snake under the Sands (Bronze)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 10,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"snake"},{"trait_type":"material","value":"copper"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nKaiwu's version of snake is designed to be a symbol of life. The plants bursting out under the scale of the snake balanced the fierce look of the snake. The snake is designed to be a protector of nature.\nOnly 3 Copper Snake is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=6","image":"http://kaiwuart.io/images/kaiwu/06sishe_ht.jpg","name":"飞砂·巳蛇(红铜) Snake under the Sands (Copper)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 11,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"snake"},{"trait_type":"material","value":"copper"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nKaiwu's version of snake is designed to be a symbol of life. The plants bursting out under the scale of the snake balanced the fierce look of the snake. The snake is designed to be a protector of nature.\nOnly 3 Copper Snake is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=6","image":"http://kaiwuart.io/images/kaiwu/06sishe_ht.jpg","name":"飞砂·巳蛇(红铜) Snake under the Sands (Copper)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 12,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"snake"},{"trait_type":"material","value":"copper"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nKaiwu's version of snake is designed to be a symbol of life. The plants bursting out under the scale of the snake balanced the fierce look of the snake. The snake is designed to be a protector of nature.\nOnly 3 Copper Snake is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=6","image":"http://kaiwuart.io/images/kaiwu/06sishe_ht.jpg","name":"飞砂·巳蛇(红铜) Snake under the Sands (Copper)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 13,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"rooster"},{"trait_type":"material","value":"gold"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nThe Kaiwu version of rooster is designed to capture the moment when it jumps from the water and stays in the air. Its wings are fully open to show the power of the jump. Zooming into the rooster's eyes, you can see the polishing is done that brings the rooster to life.\nOnly 1 Gold Rooster is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=7","image":"http://kaiwuart.io/images/kaiwu/07youji_jing.jpg","name":"弱水·酉鸡(金) Rooster above the Water (Gold)"}`,
	//}
	//artist := &NftInfo{
	//	NftKey: 14,
	//	Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"rooster"},{"trait_type":"material","value":"bronze"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nThe Kaiwu version of rooster is designed to capture the moment when it jumps from the water and stays in the air. Its wings are fully open to show the power of the jump. Zooming into the rooster's eyes, you can see the polishing is done that brings the rooster to life.\nOnly 2 Bronze Rooster is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=8","image":"http://kaiwuart.io/images/kaiwu/08youji_qt.jpg","name":"弱水·酉鸡(青铜) Rooster above the Water (Bronze)"}`,
	//}
	artist := &NftInfo{
		NftKey: 15,
		Value:`{"attributes":[{"trait_type":"collection","value":"Haiyantang"},{"trait_type":"base","value":"rooster"},{"trait_type":"material","value":"bronze"},{"trait_type":"construct","value":"3d"},{"trait_type":"from","value":"china"}],"description":"Haiyantang series, the first series of Kaiwu Art NFT.\nThe Kaiwu version of rooster is designed to capture the moment when it jumps from the water and stays in the air. Its wings are fully open to show the power of the jump. Zooming into the rooster's eyes, you can see the polishing is done that brings the rooster to life.\nOnly 2 Bronze Rooster is created. To explore more, please visit  www.kaiwuart.io","external_url":"http://kaiwuart.io/pc/singleDetail?id=8","image":"http://kaiwuart.io/images/kaiwu/08youji_qt.jpg","name":"弱水·酉鸡(青铜) Rooster above the Water (Bronze)"}`,
	}

	_,err := artist.InsertNft(GetDB())
	require.Nil(t, err)
	artists,err := QueryArtistByKey(int64(15),GetDB())
	require.Nil(t, err)

	println(artists)
}

func TestQueryArtistByKey(t *testing.T) {
	config.InitConfig("../../config_file", "dev")
	StartMysqlConnect()

	nft,err := QueryArtistByKey(int64(1),GetDB())
	require.Nil(t, err)

	println(nft.Value)
}

