/**
 * @Author: starxxliu
 * @Date: 2021/9/1 11:47 下午
 */

package mysql

import "gorm.io/gorm"

var (
	ArtistTableName = "t_kaiwu_nft"
)

type NftInfo struct {
	Id     int64  `gorm:"column:id"`
	NftKey int64 `gorm:"uniqueIndex;column:nft_key"`
	Value  string `gorm:"column:value;size:4096"`
}

func init() {
	artist := &NftInfo{}

	RegisterTable(artist)
}

func (u *NftInfo) TableName() string {
	return ArtistTableName
}

func (u *NftInfo) InsertNft(conn *gorm.DB) (*NftInfo, error) {
	err := conn.Table(ArtistTableName).Create(u).Error
	return u, err
}

func QueryArtistByKey(key int64, conn *gorm.DB) (*NftInfo, error) {
	var pro NftInfo
	err := conn.Where("nft_key = ?", key).Find(&pro).Error
	return &pro, err
}

//func DropArtist(conn *gorm.DB) error {
//	return conn.Exec("DROP TABLE t_kaiwu_nft").Error
//}
