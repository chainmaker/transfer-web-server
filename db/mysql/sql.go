/**
 * @Author: starxxliu
 * @Date: 2021/9/1 11:42 下午
 */

package mysql

import (
	"encoding/hex"
	"fmt"
	"go.uber.org/zap"
	"log"

	"chainmaker.org/chainmaker/transfer-web-server/config"
	"chainmaker.org/chainmaker/transfer-web-server/tool/crypto"
	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	mysqlDB  = &gorm.DB{}
	//因为我们需要创建many2many的表关系，所以这里用slice,需要区分创建表顺序
	tableInstance = make([]interface{}, 0)

	logger *zap.SugaredLogger
)

func StartMysqlConnect() { //mysql 重连验证
	password, _ := getPassword(config.KaiwuConfig.DBConfig.Mysql.Key, config.KaiwuConfig.DBConfig.Mysql.Password)

	sqlUrl := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", config.KaiwuConfig.DBConfig.Mysql.UserName,
		password, config.KaiwuConfig.DBConfig.Mysql.MysqlIp,
		config.KaiwuConfig.DBConfig.Mysql.MysqlPort, config.KaiwuConfig.DBConfig.Mysql.DataBase)

	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:               fmt.Sprintf("%s?charset=utf8&parseTime=True&loc=Local", sqlUrl), // DSN data source name
		DefaultStringSize: 256,
		// string 类型字段的默认长度
		DisableDatetimePrecision: true,
		// 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex: true,
		// 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn: true,
		// 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false,
		// 根据当前 MySQL 版本自动配置

	}), &gorm.Config{})

	if err != nil {
		log.Fatal("mysql db start err : " + err.Error())
	}

	mysqlDB = db

	createTable(db)
}

func StartSqliteConnect() {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	if err != nil {
		log.Fatal("mysql db start err : " + err.Error())
	}

	mysqlDB = db

	createTable(db)
}

//RegisterTable  register table
func RegisterTable(value interface{}) {
	tableInstance = append(tableInstance, value)
}

func createTable(db *gorm.DB) {
	for _, v := range tableInstance {
		if v == nil {
			continue
		}

		if !db.Migrator().HasTable(v) {
			err := db.AutoMigrate(v)
			if err != nil {
				log.Fatal("create tables failed ", err.Error())
			}
		}
	}
}

func getPassword(key, password string) (res []byte, err error) {
	keyByte, err := crypto.HashCompile(crypto.HASH_TYPE_SM3, []byte(key))
	if err != nil {
		log.Fatal(err)
	}

	pByte, err := hex.DecodeString(password)
	if err != nil {
		log.Fatal(err)
	}

	return crypto.AesDecrypto(keyByte, pByte)
}

func GetDB() *gorm.DB {
	return mysqlDB
}

func Close() {
	sqlDB, err := mysqlDB.DB()
	if err != nil {
		panic("close mysql db err: " + err.Error())
	}
	sqlDB.Close()
}

