package common

type AuthType uint16

const (
	_ AuthType = iota
	PermissionedWithCert
	PermissionedWithKey
	Public
)

var (
	AuthTypeName = map[string]AuthType{
		"permissionedWithCert": PermissionedWithCert,
		"permissionedWithKey":  PermissionedWithKey,
		"public":               Public,
	}
)
