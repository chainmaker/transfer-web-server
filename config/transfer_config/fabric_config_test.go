package transfer_config

import (
	"github.com/stretchr/testify/require"
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"os"
	"testing"
)

func TestGetStorage(t *testing.T) {
	//err := GetStorage(1,"wx-org1.chainmaker.org","uuid",nil)
	//require.Nil(t, err)
}

func TestCreateConfig(t *testing.T) {
	sqlite.StartSqliteConnect("./sqlite.db")

	defer func() {
		f, _ := os.Open("./sqlite.db")

		if f != nil {
			os.Remove("./sqlite.db")
		}
	}()

	chain := &sqlite.TargetChainInfo{
		AuthType: "public",
		OrgId: "node1",
		TaskId: "uuid",

	}

	err := CheckGenesisFile("node1","uuid",chain)
	require.Nil(t, err)

	_,err = chain.InsertTargetChainInfo(sqlite.GetDB())
	require.Nil(t, err)

	task := &sqlite.TaskInfo{
		TaskId: "uuid",
		OriginChainType: "fabric",
		OriginChainVersion: "v1.4",
		TransferHeight: 0,
	}

	_,err = task.InsertTaskInfo(sqlite.GetDB())
	require.Nil(t, err)

	oinfo := &sqlite.OriginChainInfo{
		TaskId: "uuid",
		ChannelName: "mychannel",
		UserName: "Admin1",
	}

	_,err = oinfo.InsertOriginChainInfo(sqlite.GetDB())
	require.Nil(t, err)

	_,_,err = CreateConfig("node1")
	require.Nil(t, err)
}