/**
 * @Author: xingxing
 * @Date:   2020-12-09 18:18:21
 **/

package config

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	configt "chainmaker.org/chainmaker/transfer-tool/config"
	"github.com/hokaccha/go-prettyjson"
	"github.com/spf13/viper"
)

var KaiwuViper *viper.Viper
var KaiwuConfig *Config

var (
	gEnv        string
	gConfPath   string
	ConfEnvPath string
	WhiteIp     = "white_ip"
	dev         = "dev"
	prod        = "prod"
)

// GetConfigEnv - 获取配置环境
func GetConfigEnv() string {
	var env string

	n := len(os.Args)
	for i := 1; i < n-1; i++ {
		if os.Args[i] == "-e" || os.Args[i] == "--env" {
			env = os.Args[i+1]
			break
		}
	}
	fmt.Println("[env]:", env)

	if env == "" {
		fmt.Println("env is empty, set default: dev")
		env = dev
	}

	if env == "" || (env != dev && env != "test" && env != prod) {
		fmt.Println(env + " is invalid")
		os.Exit(1)
	}

	return env
}

func InitConfig(confPath, env string) {
	gEnv = env
	gConfPath = confPath
	if gConfPath == "" {
		gConfPath = "./config_file"
	}

	var err error
	if KaiwuViper, err = initCMViper(env); err != nil {
		log.Fatal("Load config failed, ", err)
	}
	keys := KaiwuViper.AllKeys()

	fmt.Printf("%+v ", keys)

	value := KaiwuViper.Get("chain_test")
	fmt.Printf("%+v ", value)

	KaiwuConfig = &Config{}
	if err = KaiwuViper.Unmarshal(&KaiwuConfig); err != nil {
		log.Fatal("Unmarshal config failed, ", err)
	}

	//MiddlewareConfig.printLog(env)
}

func initCMViper(env string) (*viper.Viper, error) {
	//if env == "" {
	//	env = dev
	//}

	cmViper := viper.New()

	ConfEnvPath = filepath.Join(gConfPath, gEnv)

	//cmViper.SetConfigFile(ConfEnvPath + "/" + "log.yml")
	//if err := cmViper.ReadInConfig(); err != nil {
	//	//if env != "prod" {
	//	//	fmt.Printf("WARN: in [%s] can use default config, ignore err: %s\n", env, err)
	//	//	return cmViper, nil
	//	//}
	//	return nil, err
	//}

	cmViper.SetConfigFile(ConfEnvPath + "/" + "config.yml")
	if err := cmViper.MergeInConfig(); err != nil {
		return nil, err
	}

	return cmViper, nil
}

func (c *Config) printLog(env string) {
	if env == "prod" {
		return
	}

	json, err := prettyjson.Marshal(c)
	if err != nil {
		log.Fatalf("marshal alarm config failed, %s", err.Error())
	}

	fmt.Println(string(json))
}

func GetTransferConfig(path string) (*configt.Config, error) {
	cmViper := viper.New()

	cmViper.SetConfigFile(path)
	if err := cmViper.MergeInConfig(); err != nil {
		return nil, err
	}

	config := &configt.Config{}
	if err := cmViper.Unmarshal(&config); err != nil {
		log.Fatal("Unmarshal config failed, ", err)
	}

	return config, nil
}
